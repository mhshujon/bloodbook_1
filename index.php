<?php include_once 'view/include/header.php';
session_start();

if (!empty($_SESSION)) {
    if (isset($_SESSION['LoginMsg'])){
        if ($_SESSION['LoginMsg'] == 'loginFirst'){
            echo "<script>window.alert('Please Login First')</script>";
            session_destroy();
        }
    }
    if (!empty($_SESSION['RegMsg'])) {
        if (($_SESSION['RegMsg']) == 'success') {
            echo "<script>window.alert('Registration Complete!')</script>";
            echo "<script>window.alert('Please login')</script>";
            $_SESSION['RegMsg'] = '';
        }
        if (($_SESSION['RegMsg']) == 'emailExists') {
            echo "<script>window.alert('Email Already Registered!')</script>";
            $_SESSION['RegMsg'] = '';
        }
    }
}
?>

<!--Navbar -->
<nav class="navbar navbar-expand-lg navbar-danger navbar-dark" style="background-color: #cc0000; margin-bottom: 0px">
    <div class="container">
        <a class="navbar-brand" href=""><img src="assets/img/BloodBook.png" class="rounded-circle z-depth-0 logo-size" alt="avatar image"></a>
        <a class="navbar-brand" href=""><Strong>BloodBook</Strong></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4"
                aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
            <ul class="navbar-nav ml-auto">
                <?php
                if (!empty($_SESSION))
                {
                    if (isset($_SESSION['LoginMsg']))
                    {
                        if ($_SESSION['LoginMsg'] == 'invalid')
                        {
                            echo '
                                            <li class="nav-item">
                                                <a class="nav-link waves-effect waves-light" href="" data-toggle="modal" data-target="#modalLRForm">
                                                    <img src="assets/img/login.png" class="z-depth-0 logo-size" alt="avatar image">
                                                </a>
                                            </li>
                                        ';
                            echo "<script>window.alert('Wrong Information')</script>";
                            session_destroy();
                        }
                        elseif ($_SESSION['LoginMsg'] == 'success')
                        {
                            echo '
                                            <li class="nav-item">
                                                <a class="nav-link waves-effect waves-light" href="view/user/social.php?navhead=social" target="_self">
                                                    <img src="assets/img/social-platform.png" class="z-depth-0 logo-size" alt="avatar image">
                                                </a>
                                            </li>';
                            echo '
                                            <li class="nav-item">
                                                <a class="nav-link waves-effect waves-light" href="view/admin/logout.php">
                                                    <img src="assets/img/logout.png" class="z-depth-0 logo-size" alt="avatar image">
                                                </a>
                                            </li>
                                        ';
//                                        echo "<script>window.alert('Login Successful')</script>";
                        }
                        if ($_SESSION['LoginMsg'] == 'loginFirst')
                        {
                            echo '
                                            <li class="nav-item" style="">
                                                <a class="nav-link waves-effect waves-light" href="" data-toggle="modal" data-target="#modalLRForm">
                                                    <img src="assets/img/login.png" class="z-depth-0 logo-size" alt="avatar image">
                                                </a>
                                            </li>
                                        ';
                        }
                    }
                    else
                    {
                        echo '
                                    <li class="nav-item">
                                        <a class="nav-link waves-effect waves-light" href="" data-toggle="modal" data-target="#modalLRForm">
                                            <img src="assets/img/login.png" class="z-depth-0 logo-size" alt="avatar image">
                                        </a>
                                    </li>
                                ';
                    }
                }
                else
                {
                    echo '
                                    <li class="nav-item">
                                        <a class="nav-link waves-effect waves-light" href="" data-toggle="modal" data-target="#modalLRForm">
                                            <img src="assets/img/login.png" class="z-depth-0 logo-size" alt="avatar image">
                                        </a>
                                    </li>
                                ';
                }
                ?>

            </ul>
        </div>
    </div>
</nav>
<!--/.Navbar -->

<!--Modal: Login / Register Form-->
<div class="modal fade" id="modalLRForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog cascading-modal" role="document">
        <!--Content-->
        <div class="modal-content">

            <!--Modal cascading tabs-->
            <div class="modal-c-tabs">

                <!-- Nav tabs -->
                <ul style="list-style: none"  class="nav nav-tabs md-tabs tabs-2 danger-color-dark darken-3" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#panel7" role="tab"><i class="fas fa-user mr-1"></i>
                            Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#panel8" role="tab"><i class="fas fa-user-plus mr-1"></i>
                            Register</a>
                    </li>
                </ul>

                <!-- Tab panels -->
                <div class="tab-content">
                    <!--Panel 7-->
                    <div class="tab-pane fade in show active" id="panel7" role="tabpanel">

                        <!--Body-->
                        <form class="modal-body mb-1" method="POST" action="view/admin/login.php">
                            <div class="md-form form-sm mb-5">
                                <i class="fas fa-envelope prefix"></i>
                                <input required type="email" id="modalLRInput10" name="email" class="form-control form-control-sm validate">
                                <label data-error="wrong" data-success="right" for="modalLRInput10">Your email</label>
                            </div>

                            <div class="md-form form-sm mb-4">
                                <i class="fas fa-lock prefix"></i>
                                <input required type="password" id="modalLRInput11" name="pass" class="form-control form-control-sm validate">
                                <label data-error="wrong" data-success="right" for="modalLRInput11">Your password</label>
                            </div>
                            <div class="text-center mt-2">
                                <button class="btn btn-danger" type="submit">Log in <i class="fas fa-sign-in ml-1"></i></button>
                            </div>
                        </form>
                        <!--Footer-->
                        <div class="modal-footer">
                            <div class="options text-center text-md-right mt-1">
                                <p>Not a member? <a href="" class="red-text">Sign Up</a></p>
                                <p>Forgot <a href="" class="red-text">Password?</a></p>
                            </div>
                            <button type="button" class="btn btn-outline-danger waves-effect ml-auto" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                    <!--/.Panel 7-->

                    <!--Panel 8-->
                    <div class="tab-pane fade" id="panel8" role="tabpanel">

                        <!--Body-->
                        <form class="modal-body" method="POST" action="view/admin/signup.php"">
                        <div class="md-form form-sm mb-5">
                            <i class="fas fa-envelope prefix"></i>
                            <input required type="email" id="modalLRInput12" class="form-control form-control-sm validate" name="email">
                            <label data-error="Wrong" data-success="Done" for="modalLRInput12">Your email</label>
                        </div>
                        <div class="md-form form-sm mb-5">
                            <i class="fas fa-user prefix"></i>
                            <input required type="text" id="modalLRInput12" class="form-control form-control-sm validate" name="name">
                            <label data-error="Wrong" data-success="Done" for="modalLRInput12">Your Full Name</label>
                        </div>
                        <div class="md-form form-sm mb-5">
                            <i class="fas fa-question-circle prefix"></i>
                            <select required class="form-control form-control-sm validate" name="bloodGroup" style="border: unset; background-color: transparent; margin-left: 30px; max-width: 405px">
                                <option value="" disabled selected>Choose Your Blood Group</option>
                                <option value="A+">A+(ve)</option>
                                <option value="B+">B+(ve)</option>
                                <option value="O+">O+(ve)</option>
                                <option value="AB+">AB+(ve)</option>
                                <option value="A-">A-(ve)</option>
                                <option value="B-">B-(ve)</option>
                                <option value="O-">O-(ve)</option>
                                <option value="AB-">AB-(ve)</option>
                            </select>
                            <!--                                                    <input required type="text" id="modalLRInput12" class="form-control form-control-sm validate" name="bloodGroup">-->
                            <!--                                                    <label data-error="Wrong" data-success="Done" for="modalLRInput12">Choose your blood group</label>-->
                        </div>

                        <div class="md-form form-sm mb-5">
                            <i class="fas fa-lock prefix"></i>
                            <input required type="password" id="modalLRInput13" class="form-control form-control-sm validate" name="pass">
                            <label data-error="Wrong" data-success="Done" for="modalLRInput13">Your password</label>
                        </div>

                        <div class="md-form form-sm mb-4">
                            <i class="fas fa-lock prefix"></i>
                            <input required type="password" id="modalLRInput14" class="form-control form-control-sm validate">
                            <label data-error="Wrong" data-success="Done" for="modalLRInput14">Repeat password</label>
                        </div>

                        <div class="text-center form-sm mt-2">
                            <button class="btn btn-danger" type="submit">Sign up <i class="fas fa-sign-in ml-1"></i></button>
                        </div>

                        </form>
                        <!--Footer-->
                        <div class="modal-footer">
                            <div class="options text-right">
                                <p class="pt-1">Already have an account? <a href="" class="red-text">Log In</a></p>
                            </div>
                            <button type="button" class="btn btn-outline-danger waves-effect ml-auto" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!--/.Panel 8-->
                </div>

            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!--Modal: Login / Register Form-->

<!--Carousel Wrapper-->
<div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel" style="max-height: available">
    <!--Slides-->
    <div class="carousel-inner" role="listbox">
        <!--First slide-->
        <div class="carousel-item active">
            <div class="view" style="background-image: url('assets/img/bg/bg-1.jpg'); background-repeat: no-repeat; background-size: cover;">

                <!-- Mask & flexbox options-->
                <div class="mask rgba-black-light d-flex justify-content-center align-items-center">

                    <!-- Content -->
                    <div class="home-content text-center white-text mx-5 wow fadeIn">
                        <h1 class="mb-4 header">
                            <strong>“A life may depend on a gesture from you, a bottle of Blood”</strong>
                        </h1>

                        <div class="button-align col-4 col-sm-4 col-lg-4 col-xl-4">
                            <a href="view/user/learn.php" class="btn-floating round-button"><i class="fas fa-lightbulb"></i></a>
                            <div class="content">
                                <h4>LEARN</h4>
                                <p>Discover how blood donation</br>works and who it helps</p>
                            </div>
                        </div>
                        <div class="button-align col-4 col-sm-4 col-lg-4 col-xl-4">
                            <a href="" class="btn-floating round-button"><i class="fas fa-thumbs-up"></i></i></a>
                            <div class="content">
                                <h4>Eligibility</h4>
                                <p>See if you could be able to</br>donate blood</p>
                            </div>
                        </div>
                        <div class="button-align col-4 col-sm-4 col-lg-4 col-xl-4">
                            <a href="" class="btn-floating round-button"><i class="fas fa-hand-holding-usd"></i></a>
                            <div class="content">
                                <h4>Donate</h4>
                                <p>Donate blood and</br>save a life</p>
                            </div>
                        </div>

                    </div>
                    <!-- Content -->

                </div>
                <!-- Mask & flexbox options-->

            </div>
        </div>
        <!--/First slide-->

<!--        Second slide-->
        <div class="carousel-item">
            <div class="view" style="background-image: url('assets/img/bg/bg-2.jpg'); background-repeat: no-repeat; background-size: cover;">

                <!-- Mask & flexbox options-->
                <div class="mask rgba-black-light d-flex justify-content-center align-items-center">

                    <!-- Content -->
                    <div class="text-center white-text mx-5 wow fadeIn">
                        <h1 class="mb-4 header">
                            <strong>“You don’t have to be somebody’s family member to donate blood”</strong>
                        </h1>

                        <div class="button-align col-4 col-sm-4 col-lg-4 col-xl-4">
                            <a href="" class="btn-floating round-button"><i class="fas fa-lightbulb"></i></a>
                            <div class="content">
                                <h4>LEARN</h4>
                                <p>Discover how blood donation</br>works and who it helps</p>
                            </div>
                        </div>
                        <div class="button-align col-4 col-sm-4 col-lg-4 col-xl-4">
                            <a href="" class="btn-floating round-button"><i class="fas fa-thumbs-up"></i></i></a>
                            <div class="content">
                                <h4>Eligibility</h4>
                                <p>See if you could be able to</br>donate blood</p>
                            </div>
                        </div>
                        <div class="button-align col-4 col-sm-4 col-lg-4 col-xl-4">
                            <a href="" class="btn-floating round-button"><i class="fas fa-hand-holding-usd"></i></a>
                            <div class="content">
                                <h4>Donate</h4>
                                <p>Donate blood and</br>save a life</p>
                            </div>
                        </div>

                    </div>
                    <!-- Content -->

                </div>
                <!-- Mask & flexbox options-->

            </div>
        </div>
<!--        Second slide-->

<!--        Third slide-->
        <div class="carousel-item">
            <div class="view" style="background-image: url('assets/img/bg/bg-3.jpg'); background-repeat: no-repeat; background-size: cover;">

                <!-- Mask & flexbox options-->
                <div class="mask rgba-black-light d-flex justify-content-center align-items-center">

                    <!-- Content -->
                    <div class="text-center white-text mx-5 wow fadeIn">
                        <h1 class="mb-4 header">
                            <strong>“The Blood Donor of today may be recipient of tomorrow”</strong>
                        </h1>

                        <div class="button-align col-4 col-sm-4 col-lg-4 col-xl-4">
                            <a href="" class="btn-floating round-button"><i class="fas fa-lightbulb"></i></a>
                            <div class="content">
                                <h4>LEARN</h4>
                                <p>Discover how blood donation</br>works and who it helps</p>
                            </div>
                        </div>
                        <div class="button-align col-4 col-sm-4 col-lg-4 col-xl-4">
                            <a href="" class="btn-floating round-button"><i class="fas fa-thumbs-up"></i></i></a>
                            <div class="content">
                                <h4>Eligibility</h4>
                                <p>See if you could be able to</br>donate blood</p>
                            </div>
                        </div>
                        <div class="button-align col-4 col-sm-4 col-lg-4 col-xl-4">
                            <a href="" class="btn-floating round-button"><i class="fas fa-hand-holding-usd"></i></a>
                            <div class="content">
                                <h4>Donate</h4>
                                <p>Donate blood and</br>save a life</p>
                            </div>
                        </div>
                    </div>
                    <!-- Content -->

                </div>
                <!-- Mask & flexbox options-->

            </div>
        </div>
        <!--/Third slide-->

    </div>
    <!--/.Slides-->

    <!--Controls-->
    <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
<!--        <span class="carousel-control-prev-icon" aria-hidden="true"></span>-->
<!--        <span class="sr-only">Previous</span>-->
    </a>
    <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
<!--        <span class="carousel-control-next-icon" aria-hidden="true"></span>-->
<!--        <span class="sr-only">Next</span>-->
    </a>
    <!--/.Controls-->
</div>

<?php include_once 'view/include/footer.php'?>
