<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/tab-icons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
<!--    <meta http-equiv="refresh" content="1">-->
    <title>BloodBook</title>
    <base href="http://localhost/bloodbook/"/>

    <!--Icons-->
    <link rel="apple-touch-icon" sizes="57x57" href="assets/tab-icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/tab-icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/tab-icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/tab-icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/tab-icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/tab-icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/tab-icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/tab-icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/tab-icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="assets/tab-icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/tab-icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/tab-icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/tab-icons/favicon-16x16.png">
    <!--<link rel="manifest" href="assets/tab-icons/manifest.json">-->
    <!-- Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="assets/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="assets/css/style.css" rel="stylesheet">
    <style type="text/css">
        @media (min-width: 800px) and (max-width: 850px) {
            .navbar:not(.top-nav-collapse) {
                background: #1C2331 !important;
            }
        }
    </style>
</head>
<body>