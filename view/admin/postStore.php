<?php
if (empty($_POST)){
    if (empty($_SESSION))
        header('location: ../../index.php');
}
else{
    include ('../../src/Posts.php');
    session_start();

    $_POST['usersEmail'] = $_SESSION['email'];
    try{
        $object = new Posts();
        $object->set($_POST);
        $object->store();
        $_SESSION['PostMsg']='success';
        if (isset($_GET['navhead'])){
            if ($_GET['navhead'] == 'social'){
                $_SESSION['navhead'] = 'social';
                header('location: ../user/social.php');
            }
            elseif ($_GET['navhead'] == 'profile'){
                $_SESSION['navhead'] = 'profile';
                header('location: ../user/profile.php');
            }
            elseif ($_GET['navhead'] == 'viewPost'){
                $_SESSION['navhead'] = 'viewPost';
                header('location: ../user/viewPost.php');
            }
        }
//        var_dump($_POST);
    }
    catch(PDOException $ex){
        echo "<script>window.location.assign('login.php?status=dberror')</script>";
    }
}