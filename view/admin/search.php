<?php
include ('../../src/Search.php');
session_start();

try{
    $object = new Search();
    $_SESSION['searchResultUser'] = $object->searchUser($_POST['search']);
    $_SESSION['searchResultPost'] = $object->searchPost($_POST['search']);

//    echo '<br>';
    if ($_SESSION['searchResultUser'] != NULL){
//        var_dump($_SESSION['searchResultUser']);
        header('location: ../user/nothingFound.php');
    }
    elseif ($_SESSION['searchResultPost'] != NULL){
//        $_GET['postID'] = $_SESSION['searchResultPost'][]
//        for ($i=0; $i<$col; $i++){
//            $_SESSION['postID'][$i] = $_SESSION['searchResultPost'][$i]['postID'];
//        }
        $_SESSION['searchPostCount'] = $object->postCount($_POST['search']);
//        var_dump($_SESSION['searchPostCount']);
//        var_dump($_SESSION['searchResultPost'][5]['name']);
        header('location: ../user/viewSearchPost.php');
    }
    else
        header('location: ../user/nothingFound.php');
}
catch(PDOException $ex){
    echo "<script>window.location.assign('login.php?status=dberror')</script>";
}