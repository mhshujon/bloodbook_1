<?php
if (empty($_POST)){
    if (empty($_SESSION))
        header('location: ../../index.php');
}
else{
    include ('../../src/Users.php');
    session_start();

    $email = $_POST['email'];

    $object = new Users();
//var_dump($_POST);

    $eml = $object->emailValidation($email);

    try{
        if(empty($eml)){
            $object->set($_POST);
            $object->store();
            $_SESSION['RegMsg']='success';
            header('location: ../../index.php');
//            echo 'done';
        }
        else{
            $_SESSION['RegMsg'] = 'emailExists';
            header('location: ../../index.php');
//        echo 'wrong';
        }

    }
    catch(PDOException $ex){
        echo "<script>window.location.assign('login.php?status=dberror')</script>";
    }
}