<?php include_once ('../include/header.php');

session_start();
//var_dump($_SESSION);
if (!empty($_SESSION))
{
    if (isset($_SESSION['LoginMsg'])){
        if ($_SESSION['LoginMsg'] == 'loginFirst'){
            header('location: ../../index.php');
        }
    }
    if (!empty($_SESSION['UpdtMsg'])) {
        if (($_SESSION['UpdtMsg']) == 'success') {
            echo "<script>window.alert('Profile Successfully Updated!')</script>";
            $_SESSION['UpdtMsg'] = '';
        }
//        if (($_SESSION['RegMsg']) == 'emailExists') {
//            echo "<script>window.alert('Email Already Registered!')</script>";
//            $_SESSION['RegMsg'] = '';
//        }
    }
//    echo 'Yes1';
//    if (!isset($_SESSION['LoginMsg']))
//    {
//        echo 'Yes2';
//        session_destroy();
////        header('location: index.php');
//    }
    if (isset($_SESSION['LoginMsg']))
    {
//        echo 'Yes3';
//        if ($_SESSION['LoginMsg'] != 'success')
//        {
////            echo 'Yes4';
//            echo "<script>window.alert('Please Login First')</script>";
//            session_destroy();
//            header('location: ../../index.php');
//        }
        if ($_SESSION['LoginMsg'] == 'success')
        {
            include '../../src/Users.php';
            $object = new Users();
            $userEmail = $_SESSION['email'];
            $_SESSION['currentUserInfo'] = $object->currentUserInfo($userEmail);
            $_SESSION['allUserInfo'] = $object->allUserInfo();
            $_SESSION['postInfo'] = $object->postInfo();
//            var_dump($_SESSION['currentUserInfo'][0]);
//            var_dump($_SESSION['postInfo']);
//            var_dump($_SESSION['postDuration'][0]['duration']);
//            var_dump($_SESSION['allUserInfo']);
//            echo $_SESSION['allUserInfo'][1]['name'];
//            echo $_SESSION['allUserInfo'][1]['bloodGroup'];
            $countPost = $object->countPost();
            $countUser = $object->countUser();
//            var_dump($count);
            $colPost = $countPost[0]['col'];
            $colUser = $countUser[0]['col'];
//            echo $colPost;
//            var_dump($_SESSION['postInfo'][0]['bloodGroup']);

            $userFullName = $_SESSION['currentUserInfo'][0]['name'];
            $bloodGroup = $_SESSION['currentUserInfo'][0]['bloodGroup'];


        }
    }
    else{
//    echo 'Here';
        $_SESSION['LoginMsg']='loginFirst';
        header('location: ../../index.php');
    }
}
elseif (empty($_SESSION))
{
//    echo 'Here';
    $_SESSION['LoginMsg']='loginFirst';
    header('location: ../../index.php');
}

?>

<!--Navbar-->
<nav class="navbar navbar-expand-lg navbar-dark danger-color-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="index.php" target="_self"><img src="assets/img/BloodBook.png" class="rounded-circle z-depth-0 logo-size" alt="avatar image"></a>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <form class="form-inline mr-auto" action="view/admin/search.php" method="POST">
                <input required class="form-control" name="search" type="text" placeholder="Search" aria-label="Search">
            </form>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-5" aria-controls="navbarSupportedContent-5" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-5">
            <ul style="list-style: none" class="navbar-nav ml-auto nav-flex-icons">
                <li class="nav-item <?php if (isset($_GET['navhead']))if ($_GET['navhead'] == 'social') echo 'active'?>">
                    <a class="nav-link waves-effect waves-light" href="view/user/social.php?navhead=social">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item <?php if (isset($_GET['navhead']))if ($_GET['navhead'] == 'profile') echo 'active'?>">
                    <a class="nav-link waves-effect waves-light" href="view/user/profile.php?navhead=profile">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light" href="">Donors</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light">2
                        <i class="fas fa-envelope"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light">10+
                        <i class="fas fa-bell"></i>
                    </a>
                </li>
                <?php if (!empty($_SESSION)):?>
                    <?php if (isset($_SESSION['LoginMsg'])):?>
                        <?php if ($_SESSION['LoginMsg'] == 'success'):?>
                            <li class="dropdown">
                                <a class="social-nav-propic nav-link waves-effect waves-light" id="navbarDropdownMenuLink-5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <img alt="" src="assets/propic/<?php
                                    if ($_SESSION['currentUserInfo'][0]['propic'] != NULL){
                                        $_SESSION['currentPic'] = $_SESSION['currentUserInfo'][0]['propic'];
                                        echo $_SESSION['currentUserInfo'][0]['propic'];
                                    }
                                    else
                                        echo 'blank.png';
                                    ?>
                                ">
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-secondary" aria-labelledby="navbarDropdownMenuLink-5">
                                    <h6 class="us-links">Settings</h6>
                                    <ul style="list-style: none" class="us-links">
                                        <li><a href="view/user/accountSettings.php?userID=" title="">Account Setting</a></li>
                                        <li><a href="" onclick="return false" title="">Privacy</a></li>
                                        <li><a href="" onclick="return false" title="">Faqs</a></li>
                                        <li><a href="" onclick="return false" title="">Terms &amp; Conditions</a></li>
                                    </ul>
                                    <h6 class="tc"><a href="view/admin/logout.php" title="">Logout</a></h6>
                                </div>
                            </li>
                        <?php endif;?>
                    <?php endif;?>
                <?php endif;?>
            </ul>
        </div>
    </div>
</nav>

<div class="container" id="autoload">
    <div class="row padding">
        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 offset-xl-2 offset-lg-2 offset-md-2 offset-sm-2 offset-2 no-pd">
            <div class="main-ws-sec">
                <div class="table-responsive table-hover text-nowrap">
                    <form action="view/admin/profileUpdate.php" method="POST" enctype="multipart/form-data">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="font-size: larger">Account Settings</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Name:</td>
                                <td>
                                    <input required type="text" name="name" value="<?php echo $_SESSION['currentUserInfo'][0]['name'];?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Available to Donate:</td>
                                <td>
                                    <div>
                                        <input required type="radio" class="form-check-input" id="materialGroupExample1" name="availableToDonate" value="YES" <?php if ($_SESSION['currentUserInfo'][0]['availableToDonate'] == 'YES') echo 'checked'?>>
                                        <label class="form-check-label" for="materialGroupExample1">YES</label>
                                    </div>
                                    <div>
                                        <input required type="radio" class="form-check-input" id="materialGroupExample1" name="availableToDonate" value="NO" <?php if ($_SESSION['currentUserInfo'][0]['availableToDonate'] == 'NO') echo 'checked'?>>
                                        <label class="form-check-label" for="materialGroupExample1">NO</label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Email:</td>
                                <td>
                                    <input required type="text" name="email" value="<?php echo $_SESSION['currentUserInfo'][0]['email'];?>" readonly>
                                </td>
                            </tr>
                            <tr>
                                <td>Gender:</td>
                                <td>
                                    <select required class="form-control form-control-sm validate" name="gender" style="border: unset; background-color: transparent; background-color: #ececec; max-width: 250px">
                                        <option value="<?php echo $_SESSION['currentUserInfo'][0]['gender'];?>" selected><?php echo $_SESSION['currentUserInfo'][0]['gender'];?></option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
<!--                                    <input required type="text" name="gender" value="--><?php //echo $_SESSION['currentUserInfo'][0]['gender'];?><!--">-->
                                </td>
                            </tr>
                            <tr>
                                <td>Blood Group:</td>
                                <td>
                                    <select required class="form-control form-control-sm validate" name="bloodGroup" style="border: unset; background-color: transparent; background-color: #ececec; max-width: 250px">
                                        <option value="<?php echo $_SESSION['currentUserInfo'][0]['bloodGroup'];?>" selected><?php echo $_SESSION['currentUserInfo'][0]['bloodGroup'];?></option>
                                        <option value="A+">A+(ve)</option>
                                        <option value="B+">B+(ve)</option>
                                        <option value="O+">O+(ve)</option>
                                        <option value="AB+">AB+(ve)</option>
                                        <option value="A-">A-(ve)</option>
                                        <option value="B-">B-(ve)</option>
                                        <option value="O-">O-(ve)</option>
                                        <option value="AB-">AB-(ve)</option>
                                    </select>
<!--                                    <input required type="text" name="bloodGroup" value="--><?php //echo $_SESSION['currentUserInfo'][0]['bloodGroup'];?><!--">-->
                                </td>
                            </tr>
                            <tr>
                                <td>Location:</td>
                                <td>
                                    <input required type="text" name="location" value="<?php echo $_SESSION['currentUserInfo'][0]['location'];?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Work/Institution:</td>
                                <td>
                                    <input type="text" name="work_institution" value="<?php echo $_SESSION['currentUserInfo'][0]['work_institution'];?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Facebook ID URL (including: http://):</td>
                                <td>
                                    <input type="text" name="fbLink" value="<?php echo $_SESSION['currentUserInfo'][0]['fbLink'];?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Password:</td>
                                <td>
                                    <input required type="password" name="pass" value="<?php echo $_SESSION['currentUserInfo'][0]['pass'];?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Upload Profile Picture:</td>
                                <td>
                                    <input type="file" name="propic" value="<?php echo $_SESSION['currentUserInfo'][0]['pass'];?>">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <button type="submit" class="btn btn-primary flex-center"">Update Profile</button>
                    </form>
                </div>
            </div><!--main-ws-sec end-->
        </div>
    </div>

</div>

<?php include_once ('../include/footer.php');?>
