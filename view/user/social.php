<?php include_once '../../view/include/header.php';
session_start();
//var_dump($_SESSION);
if (!empty($_SESSION))
{
//    echo 'Yes1';
//    if (!isset($_SESSION['LoginMsg']))
//    {
//        echo 'Yes2';
//        session_destroy();
////        header('location: index.php');
//    }
    if (!empty($_SESSION['PostUpdtMsg'])) {
        if (($_SESSION['PostUpdtMsg']) == 'success') {
            echo "<script>window.alert('Post Successfully Updated!')</script>";
            $_SESSION['PostUpdtMsg'] = '';
        }
//        if (($_SESSION['RegMsg']) == 'emailExists') {
//            echo "<script>window.alert('Email Already Registered!')</script>";
//            $_SESSION['RegMsg'] = '';
//        }
    }
    if (isset($_SESSION['LoginMsg']))
    {
//        echo 'Yes3';
//        if ($_SESSION['LoginMsg'] != 'success')
//        {
////            echo 'Yes4';
//            echo "<script>window.alert('Please Login First')</script>";
//            session_destroy();
//            header('location: ../../index.php');
//        }
        if ($_SESSION['LoginMsg'] == 'success')
        {
            include '../../src/Users.php';
            $object = new Users();
            $userEmail = $_SESSION['email'];
            $_SESSION['currentUserInfo'] = $object->currentUserInfo($userEmail);
            $_SESSION['allUserInfo'] = $object->allUserInfo();
            $_SESSION['postInfo'] = $object->postInfo();
//            var_dump($_SESSION['postInfo']);
//            var_dump($_SESSION['postDuration'][0]['duration']);
//            var_dump($_SESSION['allUserInfo']);
//            echo $_SESSION['allUserInfo'][1]['name'];
//            echo $_SESSION['allUserInfo'][1]['bloodGroup'];
            $countPost = $object->countPost();
            $countUser = $object->countUser();
//            var_dump($count);
            $colPost = $countPost[0]['col'];
            $colUser = $countUser[0]['col'];
//            echo $colPost;
//            var_dump($_SESSION['allUserInfo'][0]['userID']);

            $userFullName = $_SESSION['currentUserInfo'][0]['name'];
            $bloodGroup = $_SESSION['currentUserInfo'][0]['bloodGroup'];
            if (!isset($_GET['navhead']))
                $_GET['navhead'] = $_SESSION['navhead'];

        }
    }
    else{
//    echo 'Here';
        $_SESSION['LoginMsg']='loginFirst';
        header('location: ../../index.php');
    }
}
elseif (empty($_SESSION))
{
//    echo 'Here';
    $_SESSION['LoginMsg']='loginFirst';
    header('location: ../../index.php');
}
?>

<!--Navbar-->
<nav class="navbar navbar-expand-lg navbar-dark danger-color-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="index.php" target="_self"><img src="assets/img/BloodBook.png" class="rounded-circle z-depth-0 logo-size" alt="avatar image"></a>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <form class="form-inline mr-auto" action="view/admin/search.php" method="POST">
                <input required class="form-control" name="search" type="text" placeholder="Search" aria-label="Search">
            </form>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-5" aria-controls="navbarSupportedContent-5" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-5">
            <ul style="list-style: none" class="navbar-nav ml-auto nav-flex-icons">
                <li class="nav-item <?php if (isset($_GET['navhead']))if ($_GET['navhead'] == 'social') echo 'active'?>">
                    <a class="nav-link waves-effect waves-light" href="view/user/social.php?navhead=social">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item <?php if (isset($_GET['navhead']))if ($_GET['navhead'] == 'profile') echo 'active'?>">
                    <a class="nav-link waves-effect waves-light" href="view/user/profile.php?navhead=profile">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light" href="" onclick="return false">Donors</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light" onclick="return false">2
                        <i class="fas fa-envelope"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light" onclick="return false">10+
                        <i class="fas fa-bell"></i>
                    </a>
                </li>
                <?php if (!empty($_SESSION)):?>
                    <?php if (isset($_SESSION['LoginMsg'])):?>
                        <?php if ($_SESSION['LoginMsg'] == 'success'):?>
                            <li class="dropdown">
                                <a class="social-nav-propic nav-link waves-effect waves-light" id="navbarDropdownMenuLink-5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <img alt="" src="assets/propic/<?php
                                    if ($_SESSION['currentUserInfo'][0]['propic'] != NULL){
                                        $_SESSION['currentUserInfo'][0]['propic'];
                                        echo $_SESSION['currentUserInfo'][0]['propic'];
                                    }
                                    else
                                        echo 'blank.png';
                                    ?>
                                    ">
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-secondary" aria-labelledby="navbarDropdownMenuLink-5">
                                    <h6 class="us-links">Settings</h6>
                                    <ul style="list-style: none" class="us-links">
                                        <li><a href="view/user/accountSettings.php?userID=" title="">Account Setting</a></li>
                                        <li><a href="" onclick="return false" title="">Privacy</a></li>
                                        <li><a href="" onclick="return false" title="">Faqs</a></li>
                                        <li><a href="" onclick="return false" title="">Terms &amp; Conditions</a></li>
                                    </ul>
                                    <h6 class="tc"><a href="view/admin/logout.php" title="">Logout</a></h6>
                                </div>
                            </li>
                        <?php endif;?>
                    <?php endif;?>
                <?php endif;?>
            </ul>
        </div>
    </div>
</nav>

<div class="container" id="autoload">
    <div class="row padding">
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3 pd-left-none no-pd">
            <div class="main-left-sidebar no-margin">
                <div class="user-data full-width">
                    <div class="user-profile">
                        <div class="username-dt">
                            <div class="usr-pic">
                                <img alt="" src="assets/propic/<?php
                                if ($_SESSION['currentUserInfo'][0]['propic'] != NULL){
                                    $_SESSION['currentUserInfo'][0]['propic'];
                                    echo $_SESSION['currentUserInfo'][0]['propic'];
                                }
                                else
                                    echo 'blank.png';
                                ?>
                                ">
                            </div>
                        </div><!--username-dt end-->
                        <div class="user-specs">
                            <h2><?php echo $userFullName?></h2>
                            <span><?php echo $bloodGroup?>(ve) Blood Donor</span>
                            <h6>Available to Donate:
                                <?php if ($_SESSION['currentUserInfo'][0]['availableToDonate'] == 'YES'):?>
                                    <i class="fas fa-check fa-fw" style="color: green"></i>
                                <?php endif;?>
                                <?php if ($_SESSION['currentUserInfo'][0]['availableToDonate'] == 'NO'):?>
                                    <i class="fas fa-times fa-fw" style="color: red"></i>
                                <?php endif;?>
                            </h6>
                        </div>
                    </div><!--user-profile end-->
                    <ul style="list-style: none" class="user-fw-status">
                        <hr>
                        <li>
                            <h4>Following</h4>
                            <?php if ($_SESSION['currentUserInfo'][0]['following'] != NULL):?>
                                <span><?php echo $_SESSION['currentUserInfo'][0]['following']?></span>
                            <?php endif;?>
                            <?php if ($_SESSION['currentUserInfo'][0]['following'] == NULL):?>
                                <span>0</span>
                            <?php endif;?>
                        </li>
                        <hr>
                        <li>
                            <h4>Followers</h4>
                            <?php if ($_SESSION['currentUserInfo'][0]['followers'] != NULL):?>
                                <span><?php echo $_SESSION['currentUserInfo'][0]['followers']?></span>
                            <?php endif;?>
                            <?php if ($_SESSION['currentUserInfo'][0]['followers'] == NULL):?>
                                <span>0</span>
                            <?php endif;?>
                        </li>
                        <hr>
                        <li>
                            <a href="view/user/profile.php?navhead=profile" title="">View Profile</a>
                        </li>
                    </ul>
                </div><!--user-data end-->
            </div><!--main-left-sidebar end-->

            <div class="suggestions full-width">
                <div class="sd-title">
                    <h3>Suggestions</h3>
                    <i class="la la-ellipsis-v"></i>
                </div><!--sd-title end-->
                <div class="suggestions-list">
                    <?php if ($colUser>0):?>
                        <?php
                        $suggUserCol = 0;
                        ?>
                        <?php for ($i=0; $i<$colUser; $i++):?>
                        <?php $suggUserCol = $suggUserCol+1?>
                            <?php if ($userEmail !=  $_SESSION['allUserInfo'][$i]['email']):?>
                                <div class="suggestion-usd">
                                    <img src="assets/propic/<?php
                                    if ($_SESSION['allUserInfo'][$i]['propic'] != NULL)
                                        echo $_SESSION['allUserInfo'][$i]['propic'];
                                    else
                                        echo 'blank.png';
                                    ?>" class="" alt="avatar image">
                                    <div class="sgt-text">
                                        <a href="view/user/viewProfile.php?userID=<?php echo $_SESSION['allUserInfo'][$i]['userID'];?>">
                                            <h4><?php echo $_SESSION['allUserInfo'][$i]['name'];?></h4>
                                        </a>
                                        <span><?php echo $_SESSION['allUserInfo'][$i]['bloodGroup'];?> Blood Donor</span>
                                    </div>
                                    <span><i class="far fa-plus-square"></i></span>
                                </div>
                            <?php endif;?>
                        <?php if ($suggUserCol == 3) break;?>
                        <?php endfor;?>
                    <?php endif;?>
<!--                    --><?php //echo $colUser?>
                    <?php if ($suggUserCol==3):?>
                        <div class="view-more">
                            <a href="" onclick="return false" title="">View More</a>
                        </div>
                    <?php endif;?>
                </div><!--suggestions-list end-->
            </div>

            <div class="tags-sec full-width">
                <ul style="list-style: none">
                    <li><a href="" onclick="return false" title="">Help Center</a></li>
                    <li><a href="" onclick="return false" title="">About</a></li>
                    <li><a href="" onclick="return false" title="">Privacy Policy</a></li>
                    <li><a href="" onclick="return false" title="">Community Guidelines</a></li>
                    <li><a href="" onclick="return false" title="">Cookies Policy</a></li>
                    <li><a href="" onclick="return false" title="">Career</a></li>
                    <li><a href="" onclick="return false" title="">Language</a></li>
                    <li><a href="" onclick="return false" title="">Copyright Policy</a></li>
                </ul>
                <div class="cp-sec">
                    <img src="assets/img/icon.png" alt="" style="max-width: 20px"> <span style="padding-left: 5px; font-size: 15px; color: #CC0000">BloodBook</span>
                    <p><i class="fas fa-copyright fa-fw"></i>Copyright 2019</p>
                </div>
            </div>
        </div>

        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 no-pd">
            <div class="main-ws-sec">
                <div class="post-topbar">
                    <div class="user-picy">
                        <img alt="" src="assets/propic/<?php
                        if ($_SESSION['currentUserInfo'][0]['propic'] != NULL){
                            $_SESSION['currentUserInfo'][0]['propic'];
                            echo $_SESSION['currentUserInfo'][0]['propic'];
                        }
                        else
                            echo 'blank.png';
                        ?>
                        ">
                    </div>
                    <div class="post-st">
                        <ul style="list-style: none">
                            <li>
                                <div class="modal fade" id="modalBloodDonateForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <form class="modal-content" method="POST" action="view/admin/postStore.php?navhead=social">
                                            <div class="modal-header text-center">
                                                <h4 class="modal-title w-100 font-weight-bold">Ask for Blood</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body mx-3">
                                                <div class="md-form mb-5">
                                                    <i class="fas fa-question-circle prefix grey-text"></i>
                                                    <select required class="form-control form-control-sm validate" name="bloodGroupNeeded" style="border: unset; background-color: transparent; margin-left: 30px; max-width: 405px">
                                                        <option value="" disabled selected>Blood Group Needed</option>
                                                        <option value="A+">A+(ve)</option>
                                                        <option value="B+">B+(ve)</option>
                                                        <option value="O+">O+(ve)</option>
                                                        <option value="AB+">AB+(ve)</option>
                                                        <option value="A-">A-(ve)</option>
                                                        <option value="B-">B-(ve)</option>
                                                        <option value="O-">O-(ve)</option>
                                                        <option value="AB-">AB-(ve)</option>
                                                    </select>
<!--                                                    <input required type="text" id="form32" class="form-control validate" name="bloodGroup">-->
<!--                                                    <label data-error="wrong" data-success="right" for="form32">Blood Group Needed</label>-->
                                                </div>

                                                <div class="md-form mb-5">
                                                    <i class="fas fa-tag prefix grey-text"></i>
                                                    <select required class="form-control form-control-sm validate" name="donorNeeded" style="border: unset; background-color: transparent; margin-left: 30px; max-width: 405px">
                                                        <option value="" disabled selected>Donor Needed</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="5+">5+</option>
                                                    </select>
<!--                                                    <input required type="text" id="form32" class="form-control validate" name="donorNeeded">-->
<!--                                                    <label data-error="wrong" data-success="right" for="form32">Donor Needed</label>-->
                                                </div>

                                                <div class="md-form mb-5">
                                                    <i class="fas fa-hospital prefix grey-text"></i>
                                                    <input required type="text" id="form34" class="form-control validate" name="hospital">
                                                    <label data-error="wrong" data-success="right" for="form34">Hospital/Clinic Name</label>
                                                </div>

                                                <div class="md-form mb-5">
                                                    <i class="fas fa-user-alt prefix grey-text"></i>
                                                    <input required type="text" id="form29" class="form-control validate" name="contactPerson">
                                                    <label data-error="wrong" data-success="right" for="form29">Contact Person Name</label>
                                                </div>

                                                <div class="md-form mb-5">
                                                    <i class="fas fa-phone prefix grey-text"></i>
                                                    <input required type="number" id="form29" maxlength="11" minlength="11" class="form-control validate" name="contactNumber">
                                                    <label data-error="wrong" data-success="right" for="form29">Contact Number</label>
                                                </div>

<!--                                                <div class="md-form">-->
<!--                                                    <i class="fas fa-pencil prefix grey-text"></i>-->
<!--                                                    <textarea type="text" id="form8" class="md-textarea form-control" rows="4" name="postDetails"></textarea>-->
<!--                                                    <label data-error="wrong" data-success="right" for="form8">Write Details</label>-->
<!--                                                </div>-->

                                            </div>
                                            <div class="modal-footer d-flex justify-content-center">
                                                <button class="btn btn-unique" type="submit">Submit <i class="fas fa-paper-plane-o ml-1"></i></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <a class="post_project" href="" title=""  data-toggle="modal" data-target="#modalBloodDonateForm">Ask for Blood</a>
                                </div>
                            </li>
                        </ul>
                    </div><!--post-st end-->
                </div><!--post-topbar end-->
                <?php if ($colPost>0):?>
                    <?php for ($i=0; $i<$colPost; $i++):?>
                        <?php $_SESSION['postUserInfo'] = $object->currentUserInfo($_SESSION['postInfo'][$i]['usersEmail']) ?>
                        <div class="posts-section">
                            <div class="post-bar">
                                <div class="post_topbar">
                                    <div class="usy-dt">
                                        <img src="assets/propic/<?php
                                        if ($_SESSION['postUserInfo'][0]['propic'] != NULL)
                                            echo $_SESSION['postUserInfo'][0]['propic'];
                                        else
                                            echo 'blank.png';
                                        ?>" class="" alt="avatar image">
<!--                                        <img src="assets/propic/--><?php //echo $_SESSION['postUserInfo'][$i]['propic'];?><!--" class="" alt="avatar image">-->
                                        <div class="usy-name">
                                            <?php if ($_SESSION['postUserInfo'][0]['email'] == $userEmail):?>
                                                <a href="view/user/profile.php?navhead=profile" target="_self"><h3><?php echo $_SESSION['postUserInfo'][0]['name']?></h3></a>
                                            <?php endif;?>
                                            <?php if ($_SESSION['postUserInfo'][0]['email'] != $userEmail):?>
                                                <a href="view/user/viewProfile.php?userID=<?php echo $_SESSION['postUserInfo'][0]['userID'];?>" target="_self"><h3><?php echo $_SESSION['postUserInfo'][0]['name']?></h3></a>
                                            <?php endif;?>
                                            <span>
                                        <i class="fas fa-clock fa-fw "></i>
                                        <?php
                                        $_SESSION['postDuration'] = $object->postDuration($_SESSION['postInfo'][$i]['usersEmail'], $_SESSION['postInfo'][$i]['postID']);
                                        $duration = $_SESSION['postDuration'][0]['duration'];
                                        if ($duration >= 60)
                                        {
                                            $duration = $duration / 60;
                                            if ($duration >= 60)
                                            {
                                                $duration = $duration / 60;
                                                if ($duration >= 24)
                                                {
                                                    $duration = $duration / 24;
                                                    if ($duration > 30)
                                                    {
                                                        $duration = $duration / 30;
                                                        if ($duration > 12)
                                                        {
                                                            $duration = $duration / 12;
                                                            if ($duration>1 && $duration<2)
                                                                echo (int)$duration.' year';
                                                            else
                                                                echo (int)$duration.' years';
                                                        }
                                                        else
                                                        {
                                                            if ($duration>1 && $duration<2)
                                                                echo (int)$duration.' month';
                                                            else
                                                                echo (int)$duration.' months';
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if ($duration>1 && $duration<2)
                                                            echo (int)$duration.' day';
                                                        else
                                                            echo (int)$duration.' days';
                                                    }
                                                }
                                                else
                                                {
                                                    if ($duration>1 && $duration<2)
                                                        echo (int)$duration.' hr';
                                                    else
                                                        echo (int)$duration.' hrs';
                                                }
                                            }
                                            else
                                            {
                                                if ($duration>1 && $duration<2)
                                                    echo (int)$duration.' min';
                                                else
                                                    echo (int)$duration.' mins';
                                            }
                                        }
                                        else
                                            echo (int)$duration.' sec';
                                        ?>
                                        ago
                                    </span>
                                        </div>
                                    </div>
                                    <?php if ($userEmail == $_SESSION['postInfo'][$i]['usersEmail']):?>
                                        <div class="modal fade" id="modalBloodDonateUpdateForm<?php echo $i?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                                             aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <form class="modal-content" method="POST" action="view/admin/postUpdate.php?id=<?php echo $_SESSION['postInfo'][$i]['postID']?>&navhead=social">
                                                    <div class="modal-header text-center">
                                                        <h4 class="modal-title w-100 font-weight-bold">Ask for Blood</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body mx-3">
                                                        <div class="md-form mb-5">
                                                            <i class="fas fa-question-circle prefix grey-text"></i>
                                                            <select required class="form-control form-control-sm validate" name="bloodGroupNeeded" style="border: unset; background-color: transparent; margin-left: 30px; max-width: 405px">
                                                                <option value="<?php echo $_SESSION['postInfo'][$i]['bloodGroupNeeded']?>" selected><?php echo $_SESSION['postInfo'][$i]['bloodGroup']?></option>
                                                                <option value="A+">A+(ve)</option>
                                                                <option value="B+">B+(ve)</option>
                                                                <option value="O+">O+(ve)</option>
                                                                <option value="AB+">AB+(ve)</option>
                                                                <option value="A-">A-(ve)</option>
                                                                <option value="B-">B-(ve)</option>
                                                                <option value="O-">O-(ve)</option>
                                                                <option value="AB-">AB-(ve)</option>
                                                            </select>
<!--                                                            <input required type="text" id="form32" class="form-control validate" name="bloodGroup" value="--><?php //echo $_SESSION['postInfo'][$i]['bloodGroup']?><!--">-->
<!--                                                            <label data-error="wrong" data-success="right" for="form32">Blood Group Needed</label>-->
                                                        </div>

                                                        <div class="md-form mb-5">
                                                            <i class="fas fa-tag prefix grey-text"></i>
                                                            <select required class="form-control form-control-sm validate" name="donorNeeded" style="border: unset; background-color: transparent; margin-left: 30px; max-width: 405px">
                                                                <option value="<?php echo $_SESSION['postInfo'][$i]['donorNeeded']?>" selected><?php echo $_SESSION['postInfo'][$i]['donorNeeded']?></option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="5+">5+</option>
                                                            </select>
<!--                                                            <input required type="text" id="form32" class="form-control validate" name="donorNeeded" value="--><?php //echo $_SESSION['postInfo'][$i]['donorNeeded']?><!--">-->
<!--                                                            <label data-error="wrong" data-success="right" for="form32">Donor Needed</label>-->
                                                        </div>

                                                        <div class="md-form mb-5">
                                                            <i class="fas fa-hospital prefix grey-text"></i>
                                                            <input required type="text" id="form34" class="form-control validate" name="hospital" value="<?php echo $_SESSION['postInfo'][$i]['hospital']?>">
                                                            <label data-error="wrong" data-success="right" for="form34">Hospital/Clinic Name</label>
                                                        </div>

                                                        <div class="md-form mb-5">
                                                            <i class="fas fa-user-alt prefix grey-text"></i>
                                                            <input required type="text" id="form29" class="form-control validate" name="contactPerson" value="<?php echo $_SESSION['postInfo'][$i]['contactPerson']?>">
                                                            <label data-error="wrong" data-success="right" for="form29">Contact Person Name</label>
                                                        </div>

                                                        <div class="md-form mb-5">
                                                            <i class="fas fa-phone prefix grey-text"></i>
                                                            <input required type="number" id="form29" maxlength="11" minlength="11" class="form-control validate" name="contactNumber" value="<?php echo $_SESSION['postInfo'][$i]['contactNumber']?>">
                                                            <label data-error="wrong" data-success="right" for="form29">Contact Number</label>
                                                        </div>

<!--                                                        <div class="md-form">-->
<!--                                                            <i class="fas fa-pencil prefix grey-text"></i>-->
<!--                                                            <textarea type="text" id="form8" class="md-textarea form-control" rows="4" name="postDetails">-->
<!--                                                                --><?php
//                                                                    if ($_SESSION['postInfo'][$i]['postDetails'] != NULL)
//                                                                        echo $_SESSION['postInfo'][$i]['postDetails']
//                                                                ?>
<!--                                                            </textarea>-->
<!--                                                            <label data-error="wrong" data-success="right" for="form8">Write Details</label>-->
<!--                                                        </div>-->

                                                    </div>
                                                    <div class="modal-footer d-flex justify-content-center">
                                                        <button class="btn btn-unique" type="submit">Update <i class="fas fa-paper-plane-o ml-1"></i></button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="dropdown ed-opts">
                                            <a href="" onclick="return false" title="" class="dropdown-toggle" data-toggle="dropdown"
                                               aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v" data-toggle="dropdown"></i></a>
                                            <div class="dropdown-menu" aria-labelledby="">
                                                <button class="dropdown-item" type="button"  data-toggle="modal" data-target="#modalBloodDonateUpdateForm<?php echo $i?>">Edit Post</button>
                                                <a href="view/admin/deletePost.php?postID=<?php echo $_SESSION['postInfo'][$i]['postID']?>&navhead=social">
                                                    <button class="dropdown-item" type="button">Delete Post</button>
                                                </a>
                                            </div>
                                        </div>
                                    <?php endif;?>
                                </div>
                                <div class="epi-sec">
                                    <ul style="list-style: none" class="descp">
                                        <li><i class="fas fa-globe-americas"></i></li>
                                        <span>Dhaka, Bangladesh</span>
                                    </ul>
                                    <ul style="list-style: none" class="bk-links">
                                        <li><a href="" onclick="return false" title=""><i class="far fa-bookmark"></i></a></li>
                                        <li><a href="" onclick="return false" title=""><i class="far fa-envelope"></i></a></li>
                                    </ul>
                                </div>
                                <div class="job_descp">
                                    <a href="view/user/viewPost.php?postID=<?php echo $_SESSION['postInfo'][$i]['postID']?>" target="_self">
                                        <h3><?php echo $_SESSION['postInfo'][$i]['bloodGroupNeeded']?>(ve) Blood Needed</h3>
                                    </a>
                                    <!--                            <ul style="list-style: none" class="job-dt">-->
                                    <!--                            <li><a href="" title="">Full Time</a></li>-->
                                    <!--                            <li><span>$30 / hr</span></li>-->
                                    <!--                            </ul>-->
                                    <ul style="list-style: none" class="mandatory-post-info">
                                        <li>Donor Needed: <?php echo $_SESSION['postInfo'][$i]['donorNeeded']?></li>
                                        <li>Location: <?php echo $_SESSION['postInfo'][$i]['hospital']?></li>
                                        <li>Contact Person: <?php echo $_SESSION['postInfo'][$i]['contactPerson']?></li>
                                        <li>Contact Number: <?php echo $_SESSION['postInfo'][$i]['contactNumber']?></li>
                                    </ul>
                                    <?php if ($_SESSION['postInfo'][$i]['postDetails'] != NULL):?>
                                        <p><?php echo $_SESSION['postInfo'][$i]['postDetails'];?> <a href="" onclick="return false" title="">view more</a></p>
                                    <?php endif;?>
                                </div>
                                <?php
                                    $_SESSION['likes'] = $object->countLikes($_SESSION['postInfo'][$i]['postID']);
                                    $doneLike = $object->doneLike($_SESSION['postInfo'][$i]['postID'], $_SESSION['currentUserInfo'][0]['userID']);
//                                    var_dump($doneLike) ;
                                ?>
                                <div class="job-status-bar">
                                    <ul style="list-style: none" class="like-com">
                                        <li>
                                            <?php if ($doneLike == NULL):?>
                                            <a href="view/admin/likeStore.php?postID=<?php echo $_SESSION['postInfo'][$i]['postID']?>&userID=<?php echo $_SESSION['currentUserInfo'][0]['userID']?>&postUserID=<?php echo $_SESSION['postUserInfo'][0]['userID']?>&navhead=social"><i class="far fa-thumbs-up"></i><?php if ($_SESSION['likes'][0]['totalLikes'] != 0) echo $_SESSION['likes'][0]['totalLikes'];?> Like
                                            </a>
                                            <?php endif;?>

                                            <?php if ($doneLike != NULL):?>
                                            <a href="view/admin/likeDelete.php?postID=<?php echo $_SESSION['postInfo'][$i]['postID']?>&userID=<?php echo $_SESSION['currentUserInfo'][0]['userID']?>&postUserID=<?php echo $_SESSION['postUserInfo'][0]['userID']?>&navhead=social" style="color: #CC0000;"><i class="far fa-thumbs-up"></i><?php echo $_SESSION['likes'][0]['totalLikes'];?> Like
                                            </a>
                                            <?php endif;?>
                                        </li>
                                        <li>
                                            <?php
                                            //                                    var_dump($_SESSION['postInfo'][$i]['postID']);
                                            $commentPostID = $_SESSION['postInfo'][$i]['postID'];
                                            $countComment = $object->countComment($_SESSION['postInfo'][$i]['postID']);
                                            //                                    var_dump($countComment);
                                            $colComment = $countComment[0]['col'];
                                            //                                                                        var_dump($colComment);
                                            //                                    var_dump($_SESSION['postInfo'][$i]['postID']);
                                            //                                    var_dump($email = $_SESSION['allUserInfo'][$i]['email']);
                                            ?>
                                            <a href="" title="" class="com" data-toggle="dropdown"
                                               aria-haspopup="true" aria-expanded="false"><i class="far fa-comments"></i>
                                               <?php
                                                   if ($colComment == 0)
                                                       echo ' Comment';
                                                   elseif ($colComment>10)
                                                       echo '10+ Comments';
                                                   elseif ($colComment==1)
                                                       echo $colComment.' Comment';
                                                   else
                                                       echo $colComment.' Comments';
                                               ?>
                                            </a>
                                            <div class="comment-section dropdown-menu" style="margin-top: 18px; margin-left: -92px; max-width: 540px">
                                                <!--                                        <div class="plus-ic">-->
                                                <!--                                            <i class="fa fa-plus"></i>-->
                                                <!--                                        </div>-->
                                                <div class="comment-sec dropdown-wrapper-scroll-y my-custom-scrollbar">
                                                    <ul style="list-style: none">
                                                        <?php if($colComment>0):?>
                                                            <?php for ($j=0; $j<$colComment; $j++):?>
                                                                <?php
                                                                //                                                                $email = $_SESSION['allUserInfo'][$i]['email'];
                                                                //                                                                var_dump($email);
                                                                $_SESSION['commentInfo'] = $object->commentInfo($commentPostID);
                                                                //                                                                var_dump($_SESSION['commentInfo'])
                                                                ?>
                                                                <li>
                                                                    <div class="comment-list">
                                                                        <div class="cmnt-img">
                                                                            <img src="assets/propic/<?php
                                                                            if ($_SESSION['commentInfo'][$j]['propic'] != NULL)
                                                                                echo $_SESSION['commentInfo'][$j]['propic'];
                                                                            else
                                                                                echo 'blank.png';
                                                                            ?>" class="" alt="avatar image">
<!--                                                                            <img src="assets/propic/--><?php //echo $_SESSION['commentInfo'][$j]['propic'];?><!--" class="" alt="avatar image">-->
                                                                        </div>
                                                                        <div class="comment">
                                                                            <!--                                                            <span><i class="fas fa-clock fa-fw"></i> 3 min ago</span>-->
                                                                            <h3><?php echo $_SESSION['commentInfo'][$j]['name']?></h3><i class="fas fa-clock fa-fw float-left" style="color: #c0c0c0; font-size: 12px"></i>
                                                                            <h6 style="font-size: 12px">
                                                                                <?php
                                                                                $_SESSION['commentDuration'] = $object->commentDuration($_SESSION['commentInfo'][$j]['commentID'], $commentPostID);
                                                                                $duration = $_SESSION['commentDuration'][0]['duration'];
                                                                                if ($duration >= 60)
                                                                                {
                                                                                    $duration = $duration / 60;
                                                                                    if ($duration >= 60)
                                                                                    {
                                                                                        $duration = $duration / 60;
                                                                                        if ($duration >= 24)
                                                                                        {
                                                                                            $duration = $duration / 24;
                                                                                            if ($duration > 30)
                                                                                            {
                                                                                                $duration = $duration / 30;
                                                                                                if ($duration > 12)
                                                                                                {
                                                                                                    $duration = $duration / 12;
                                                                                                    if ($duration>1 && $duration<2)
                                                                                                        echo (int)$duration.' year';
                                                                                                    else
                                                                                                        echo (int)$duration.' years';
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    if ($duration>1 && $duration<2)
                                                                                                        echo (int)$duration.' month';
                                                                                                    else
                                                                                                        echo (int)$duration.' months';
                                                                                                }
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                if ($duration>1 && $duration<2)
                                                                                                    echo (int)$duration.' day';
                                                                                                else
                                                                                                    echo (int)$duration.' days';
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            if ($duration>1 && $duration<2)
                                                                                                echo (int)$duration.' hr';
                                                                                            else
                                                                                                echo (int)$duration.' hrs';
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        if ($duration>1 && $duration<2)
                                                                                            echo (int)$duration.' min';
                                                                                        else
                                                                                            echo (int)$duration.' mins';
                                                                                    }
                                                                                }
                                                                                else
                                                                                    echo (int)$duration.' sec';
                                                                                ?>
                                                                                ago
                                                                            </h6>
                                                                            <p style="margin-top: 5px; color: #4b4b4b">
                                                                                <?php echo $_SESSION['commentInfo'][$j]['comments']?>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <!--                                        comment-list end-->
                                                                </li>
                                                            <?php endfor;?>
                                                        <?php endif;?>
                                                    </ul>
                                                </div>
                                                <!--                            comment-sec end-->
                                                <div class="post-comment">
                                                    <!--                                <div class="cm_img">-->
                                                    <!--                                    <img src="assets/img/navbar-propic.png" class="rounded-circle z-depth-0 size" alt="avatar image">-->
                                                    <!--                                </div>-->
                                                    <div class="comment_box">
                                                        <form action="view/admin/storeComments.php?postID=<?php echo $commentPostID?>&navhead=social" method="POST">
                                                            <input required type="text" placeholder="Post a comment" name="comments">
                                                            <button type="submit">Send</button>
                                                        </form>
                                                    </div>
                                                </div>
                                                <!--                            post-comment end-->
                                            </div>
                                        </li>
                                        <li>
                                            <a href="" onclick="return false" title="" class="com"><i class="fas fa-share-square"></i> Share</a>
<!--                                            <iframe src="https://www.facebook.com/plugins/share_button.php?href=http%3A%2F%2Fwww.bloodbook.live%2Fview%2Fuser%2FviewPost.php%3FpostID%3D--><?php //echo $_SESSION['postInfo'][$i]['postID']?><!--&layout=button_count&size=small&appId=499356067077040&width=69&height=20" width="69" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"><i class="fas fa-share-square"></i></iframe>-->
                                        </li>
                                    </ul>
                                    <a style="color: #b7b7b7"><i class="fas fa-eye"></i>Views 1</a>
                                </div>
                                <!--                        Comment Section-->
                                <!--                        <div class="dropdown ed-opts">-->
                                <!--                            <a href="" onclick="return false" title="" class="dropdown-toggle" data-toggle="dropdown"-->
                                <!--                               aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v" data-toggle="dropdown"></i></a>-->
                                <!--                            <div class="dropdown-menu" aria-labelledby="">-->
                                <!--                                <button class="dropdown-item" type="button"  data-toggle="modal" data-target="#modalBloodDonateUpdateForm--><?php //echo $i?><!--">Edit Post</button>-->
                                <!--                                <button class="dropdown-item" type="button">Unsave Post</button>-->
                                <!--                                <button class="dropdown-item" type="button">Hide</button>-->
                                <!--                            </div>-->
                                <!--                        </div>-->
                            </div>
                        </div>
                    <?php endfor;?>
                <?php endif;?>
            </div><!--main-ws-sec end-->
        </div>

        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3 pd-right-none no-pd">
            <div class="right-sidebar">
                <div class="widget suggestions full-width">
                    <div class="sd-title">
                        <h3>Top Donors [All Time]</h3>
                        <i class="la la-ellipsis-v"></i>
                    </div><!--sd-title end-->
                    <div class="suggestions-list">
                        <?php if ($colUser>0):?>
                            <?php
                            $topDonorsCol = 0;
                            ?>
                            <?php for ($i=0; $i<$colUser; $i++):?>
                            <?php $topDonorsCol = $topDonorsCol+1?>
                                <div class="suggestion-usd">
                                    <img src="assets/propic/<?php
                                    if ($_SESSION['allUserInfo'][$i]['propic'] != NULL)
                                        echo $_SESSION['allUserInfo'][$i]['propic'];
                                    else
                                        echo 'blank.png';
                                    ?>" class="" alt="avatar image">
                                    <div class="sgt-text">
                                        <?php if ($_SESSION['allUserInfo'][$i]['email'] == $userEmail):?>
                                            <a href="view/user/profile.php?navhead=profile" target="_self"><h4><?php echo $_SESSION['allUserInfo'][$i]['name']?></h4></a>
                                        <?php endif;?>
                                        <?php if ($_SESSION['allUserInfo'][$i]['email'] != $userEmail):?>
                                            <a href="view/user/viewProfile.php?userID=<?php echo $_SESSION['allUserInfo'][$i]['userID'];?>" target="_self"><h4><?php echo $_SESSION['allUserInfo'][$i]['name']?></h4></a>
                                        <?php endif;?>
<!--                                        <a href="view/user/viewProfile.php?userID=--><?php //echo $_SESSION['allUserInfo'][$i]['userID'];?><!--">-->
<!--                                            <h4>--><?php //echo $_SESSION['allUserInfo'][$i]['name'];?><!--</h4>-->
<!--                                        </a>-->
                                        <span><?php echo $_SESSION['allUserInfo'][$i]['bloodGroup'];?> Blood Donor</span>
                                    </div>
                                    <span><i class="far fa-plus-square"></i></span>
                                </div>
                            <?php if ($topDonorsCol == 3) break;?>
                            <?php endfor;?>
                        <?php endif;?>
                        <?php if ($topDonorsCol==3):?>
                            <div class="view-more">
                                <a href="" onclick="return false" title="">View More</a>
                            </div>
                        <?php endif;?>
                    </div><!--suggestions-list end-->
                </div>

                <div class="widget suggestions full-width">
                    <div class="sd-title">
                        <h3>Available To Donate</h3>
                        <i class="la la-ellipsis-v"></i>
                    </div><!--sd-title end-->
                    <div class="suggestions-list">
                        <?php if ($colUser>0):?>
                            <?php
                            $availableToDonateCol = 0;
                            ?>
                            <?php for ($i=0; $i<$colUser; $i++):?>
                                <?php if ($userEmail !=  $_SESSION['allUserInfo'][$i]['email']):?>
                                    <?php if ($_SESSION['allUserInfo'][$i]['availableToDonate'] == 'YES'):?>
                                    <?php $availableToDonateCol=$availableToDonateCol+1?>
                                        <div class="suggestion-usd">
                                            <img src="assets/propic/<?php
                                            if ($_SESSION['allUserInfo'][$i]['propic'] != NULL)
                                                echo $_SESSION['allUserInfo'][$i]['propic'];
                                            else
                                                echo 'blank.png';
                                            ?>" class="" alt="avatar image">
                                            <div class="sgt-text">
                                                <a href="view/user/viewProfile.php?userID=<?php echo $_SESSION['allUserInfo'][$i]['userID'];?>">
                                                    <h4><?php echo $_SESSION['allUserInfo'][$i]['name'];?></h4>
                                                </a>
                                                <span><?php echo $_SESSION['allUserInfo'][$i]['bloodGroup'];?> Blood Donor</span>
                                            </div>
                                            <span><i class="far fa-plus-square"></i></span>
                                        </div>
                                        <?php if ($availableToDonateCol==3):?>
                                            <div class="view-more">
                                                <a href="" onclick="return false" title="">View More</a>
                                            </div>
                                        <?php endif;?>
                                    <?php endif;?>
                                <?php endif;?>
                            <?php if ($availableToDonateCol == 3) break; ?>
                            <?php endfor;?>
                        <?php endif;?>
                    </div><!--suggestions-list end-->
                </div>
            </div><!--right-sidebar end-->
        </div>

    </div>

</div>

<?php include_once '../../view/include/footer.php'?>
