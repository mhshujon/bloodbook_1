<?php include_once ('../include/header.php');

session_start();

if (!empty($_SESSION)) {
    if (isset($_SESSION['LoginMsg'])){
        if ($_SESSION['LoginMsg'] == 'loginFirst'){
            echo "<script>window.alert('Please Login First')</script>";
            session_destroy();
        }
    }
    if (!empty($_SESSION['RegMsg'])) {
        if (($_SESSION['RegMsg']) == 'success') {
            echo "<script>window.alert('Registration Complete!')</script>";
            echo "<script>window.alert('Please login')</script>";
            $_SESSION['RegMsg'] = '';
        }
        if (($_SESSION['RegMsg']) == 'emailExists') {
            echo "<script>window.alert('Email Already Registered!')</script>";
            $_SESSION['RegMsg'] = '';
        }
    }
}
?>

    <!--Navbar -->
    <nav class="navbar navbar-expand-lg navbar-danger navbar-dark" style="background-color: #cc0000; margin-bottom: 0px">
        <div class="container">
            <a class="navbar-brand" href=""><img src="assets/img/BloodBook.png" class="rounded-circle z-depth-0 logo-size" alt="avatar image"></a>
            <a class="navbar-brand" href=""><Strong>BloodBook</Strong></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4"
                    aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
                <ul class="navbar-nav ml-auto">
                    <?php
                    if (!empty($_SESSION))
                    {
                        if (isset($_SESSION['LoginMsg']))
                        {
                            if ($_SESSION['LoginMsg'] == 'invalid')
                            {
                                echo '
                                            <li class="nav-item">
                                                <a class="nav-link waves-effect waves-light" href="" data-toggle="modal" data-target="#modalLRForm">
                                                    <img src="assets/img/login.png" class="z-depth-0 logo-size" alt="avatar image">
                                                </a>
                                            </li>
                                        ';
                                echo "<script>window.alert('Wrong Information')</script>";
                                session_destroy();
                            }
                            elseif ($_SESSION['LoginMsg'] == 'success')
                            {
                                echo '
                                            <li class="nav-item">
                                                <a class="nav-link waves-effect waves-light" href="view/user/social.php?navhead=social" target="_self">
                                                    <img src="assets/img/social-platform.png" class="z-depth-0 logo-size" alt="avatar image">
                                                </a>
                                            </li>';
                                echo '
                                            <li class="nav-item">
                                                <a class="nav-link waves-effect waves-light" href="view/admin/logout.php">
                                                    <img src="assets/img/logout.png" class="z-depth-0 logo-size" alt="avatar image">
                                                </a>
                                            </li>
                                        ';
//                                        echo "<script>window.alert('Login Successful')</script>";
                            }
                            if ($_SESSION['LoginMsg'] == 'loginFirst')
                            {
                                echo '
                                            <li class="nav-item" style="">
                                                <a class="nav-link waves-effect waves-light" href="" data-toggle="modal" data-target="#modalLRForm">
                                                    <img src="assets/img/login.png" class="z-depth-0 logo-size" alt="avatar image">
                                                </a>
                                            </li>
                                        ';
                            }
                        }
                        else
                        {
                            echo '
                                    <li class="nav-item">
                                        <a class="nav-link waves-effect waves-light" href="" data-toggle="modal" data-target="#modalLRForm">
                                            <img src="assets/img/login.png" class="z-depth-0 logo-size" alt="avatar image">
                                        </a>
                                    </li>
                                ';
                        }
                    }
                    else
                    {
                        echo '
                                    <li class="nav-item">
                                        <a class="nav-link waves-effect waves-light" href="" data-toggle="modal" data-target="#modalLRForm">
                                            <img src="assets/img/login.png" class="z-depth-0 logo-size" alt="avatar image">
                                        </a>
                                    </li>
                                ';
                    }
                    ?>

                </ul>
            </div>
        </div>
    </nav>
    <!--/.Navbar -->

    <!--Modal: Login / Register Form-->
    <div class="modal fade" id="modalLRForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog cascading-modal" role="document">
            <!--Content-->
            <div class="modal-content">

                <!--Modal cascading tabs-->
                <div class="modal-c-tabs">

                    <!-- Nav tabs -->
                    <ul style="list-style: none"  class="nav nav-tabs md-tabs tabs-2 danger-color-dark darken-3" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#panel7" role="tab"><i class="fas fa-user mr-1"></i>
                                Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#panel8" role="tab"><i class="fas fa-user-plus mr-1"></i>
                                Register</a>
                        </li>
                    </ul>

                    <!-- Tab panels -->
                    <div class="tab-content">
                        <!--Panel 7-->
                        <div class="tab-pane fade in show active" id="panel7" role="tabpanel">

                            <!--Body-->
                            <form class="modal-body mb-1" method="POST" action="view/admin/login.php">
                                <div class="md-form form-sm mb-5">
                                    <i class="fas fa-envelope prefix"></i>
                                    <input required type="email" id="modalLRInput10" name="email" class="form-control form-control-sm validate">
                                    <label data-error="wrong" data-success="right" for="modalLRInput10">Your email</label>
                                </div>

                                <div class="md-form form-sm mb-4">
                                    <i class="fas fa-lock prefix"></i>
                                    <input required type="password" id="modalLRInput11" name="pass" class="form-control form-control-sm validate">
                                    <label data-error="wrong" data-success="right" for="modalLRInput11">Your password</label>
                                </div>
                                <div class="text-center mt-2">
                                    <button class="btn btn-danger" type="submit">Log in <i class="fas fa-sign-in ml-1"></i></button>
                                </div>
                            </form>
                            <!--Footer-->
                            <div class="modal-footer">
                                <div class="options text-center text-md-right mt-1">
                                    <p>Not a member? <a href="" class="red-text">Sign Up</a></p>
                                    <p>Forgot <a href="" class="red-text">Password?</a></p>
                                </div>
                                <button type="button" class="btn btn-outline-danger waves-effect ml-auto" data-dismiss="modal">Close</button>
                            </div>

                        </div>
                        <!--/.Panel 7-->

                        <!--Panel 8-->
                        <div class="tab-pane fade" id="panel8" role="tabpanel">

                            <!--Body-->
                            <form class="modal-body" method="POST" action="view/admin/signup.php"">
                            <div class="md-form form-sm mb-5">
                                <i class="fas fa-envelope prefix"></i>
                                <input required type="email" id="modalLRInput12" class="form-control form-control-sm validate" name="email">
                                <label data-error="Wrong" data-success="Done" for="modalLRInput12">Your email</label>
                            </div>
                            <div class="md-form form-sm mb-5">
                                <i class="fas fa-user prefix"></i>
                                <input required type="text" id="modalLRInput12" class="form-control form-control-sm validate" name="name">
                                <label data-error="Wrong" data-success="Done" for="modalLRInput12">Your Full Name</label>
                            </div>
                            <div class="md-form form-sm mb-5">
                                <i class="fas fa-question-circle prefix"></i>
                                <select required class="form-control form-control-sm validate" name="bloodGroup" style="border: unset; background-color: transparent; margin-left: 30px; max-width: 405px">
                                    <option value="" disabled selected>Choose Your Blood Group</option>
                                    <option value="A+">A+(ve)</option>
                                    <option value="B+">B+(ve)</option>
                                    <option value="O+">O+(ve)</option>
                                    <option value="AB+">AB+(ve)</option>
                                    <option value="A-">A-(ve)</option>
                                    <option value="B-">B-(ve)</option>
                                    <option value="O-">O-(ve)</option>
                                    <option value="AB-">AB-(ve)</option>
                                </select>
                                <!--                                                    <input required type="text" id="modalLRInput12" class="form-control form-control-sm validate" name="bloodGroup">-->
                                <!--                                                    <label data-error="Wrong" data-success="Done" for="modalLRInput12">Choose your blood group</label>-->
                            </div>

                            <div class="md-form form-sm mb-5">
                                <i class="fas fa-lock prefix"></i>
                                <input required type="password" id="modalLRInput13" class="form-control form-control-sm validate" name="pass">
                                <label data-error="Wrong" data-success="Done" for="modalLRInput13">Your password</label>
                            </div>

                            <div class="md-form form-sm mb-4">
                                <i class="fas fa-lock prefix"></i>
                                <input required type="password" id="modalLRInput14" class="form-control form-control-sm validate">
                                <label data-error="Wrong" data-success="Done" for="modalLRInput14">Repeat password</label>
                            </div>

                            <div class="text-center form-sm mt-2">
                                <button class="btn btn-danger" type="submit">Sign up <i class="fas fa-sign-in ml-1"></i></button>
                            </div>

                            </form>
                            <!--Footer-->
                            <div class="modal-footer">
                                <div class="options text-right">
                                    <p class="pt-1">Already have an account? <a href="" class="red-text">Log In</a></p>
                                </div>
                                <button type="button" class="btn btn-outline-danger waves-effect ml-auto" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                        <!--/.Panel 8-->
                    </div>

                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!--Modal: Login / Register Form-->

    <!-- Page header section start -->
    <section class="page-header-section set-bg" style="background-image: url('assets/img/bg/Learn-bg.jpg'); background-repeat: no-repeat; background-size: cover;">
        <div class="container">
            <h1 class="header-title">Learn<span>.</span></h1>
        </div>
    </section>
    <!-- Page header section end -->

    <!-- Intro section start -->
    <section class="intro-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 intro-text">
                    <h2 style="font-size: 35px; font-weight: bold; margin-bottom: 20px; line-height: 40px">The Benefits of Donating Blood</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                There’s no end to the benefits of donating blood for those who need it. According to the
                                <a href="https://rcblood.org/2RUP86t" style="color: #cc0000" target="_blank">American Red Cross</a>, one donation can save as many as three lives.
                            </p>
                        </div>
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                It turns out that donating blood doesn’t just benefit recipients. There are health benefits for donors, too, on top of the benefits that come from helping others. Read on to learn the health benefits of donating blood and the reasons behind them.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 intro-text">
                    <h2 style="font-size: 35px; font-weight: bold; margin-bottom: 20px; line-height: 40px">Benefits</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                Donating blood has benefits for your emotional and physical health. According to a report by the
                                <a href="https://bit.ly/2Z7Sfvw" style="color: #cc0000" target="_blank">Mental Health Foundation</a>, helping others can:
                            </p>
                        </div>
                        <div class="col-md-12" style="margin-bottom: 30px">
                            <ul>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Reduce stress</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Improve your emotional well-being</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Benefit your physical health</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Help get rid of negative feelings</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Provide a sense of belonging and reduce isolation</li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                Research has found further evidence of the health benefits that come specifically from donating blood.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 intro-text">
                    <h2 style="font-size: 35px; font-weight: bold; margin-bottom: 20px; line-height: 40px">Lower risk of heart disease</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                Blood donation may lower the risk of heart disease and heart attack. This is because it reduces the blood’s viscosity.
                            </p>
                        </div>
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                A <a href="https://bit.ly/2UMFmrD" style="color: #cc0000" target="_blank">2013 study</a> found that regular blood donation significantly lowered the mean total cholesterol and low-density lipoprotein cholesterol, protecting against cardiovascular disease. Researchers note this is consistent with findings in other studies which found that blood donors had a lower risk of heart disease and heart attack.
                            </p>
                        </div>
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                Donating blood regularly may also <a href="https://bit.ly/2UMFmrD" style="color: #cc0000" target="_blank">lower</a> iron stores. This <a href="https://bit.ly/2VBMaFR" style="color: #cc0000" target="_blank">lower</a> reduce the risk of heart attack. High body iron stores are believed to increase the risk of heart attack.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 intro-text">
                    <h2 style="font-size: 35px; font-weight: bold; margin-bottom: 20px; line-height: 40px">Lower risk of cancer</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                A <a href="https://bit.ly/2VBMaFR" style="color: #cc0000" target="_blank">2008 study</a> found a small decrease in the risk of certain cancers in people who regularly donated blood. These included cancers that are linked to high iron levels, including cancer of the:
                            </p>
                        </div>
                        <div class="col-md-12" style="margin-bottom: 30px">
                            <ul>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Liver</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Colon</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Lung</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Esophagus</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Stomach</li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                A <a href="https://bit.ly/2URVLuM" style="color: #cc0000" target="_blank">2016 study</a> also found that donating blood can lower inflammatory markers and increase antioxidant capacity.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 intro-text">
                    <h2 style="font-size: 35px; font-weight: bold; margin-bottom: 20px; line-height: 40px">Free health checkup</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                In order to give blood, you’re required to undergo a health screening. A trained staff member performs this checkup. They’ll check your:
                            </p>
                        </div>
                        <div class="col-md-12" style="margin-bottom: 30px">
                            <ul>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Pulse</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Blood pressure</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Body temperature</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Hemoglobin levels</li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                This free mini-physical can offer excellent insight into your health. It can effectively detect problems that could indicate an underlying medical condition or risk factors for certain diseases.
                            </p>
                        </div>
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                Your blood is also tested for several diseases. These include:
                            </p>
                        </div>
                        <div class="col-md-12" style="margin-bottom: 30px">
                            <ul>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Hepatitis B</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Hepatitis C</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">HIV</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">West Nile virus</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Syphilis</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Trypanosoma cruzi</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 intro-text">
                    <h2 style="font-size: 35px; font-weight: bold; margin-bottom: 20px; line-height: 40px">Side effects of donating blood</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                Blood donation is safe for healthy adults. There’s no risk of contracting disease. New, sterile equipment is used for each donor.
                            </p>
                        </div>
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                Some people may feel nauseous, lightheaded, or dizzy after donating blood. If this happens, it should only last a few minutes. You can lie down with your feet up at the until you feel better.
                            </p>
                        </div>
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                You may also experience some bleeding at the site of the needle. Applying pressure and raising your arm for a couple of minutes will usually stop this. You may develop a bruise at the site.
                            </p>
                        </div>
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                Call the blood donation center if:
                            </p>
                        </div>
                        <div class="col-md-12" style="margin-bottom: 30px">
                            <ul>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">You still feel lightheaded, dizzy, or nauseous after drinking, eating, and resting.</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">You develop a raised bump or continue bleeding at the needle site.</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">You have arm pain, numbness, or tingling.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 intro-text">
                    <h2 style="font-size: 35px; font-weight: bold; margin-bottom: 20px; line-height: 40px">During the donation</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                You must register to donate blood. This includes providing identification, your medical history, and undergoing a quick physical examination. You’ll also be given some information about blood donation to read.
                            </p>
                        </div>
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                Once you’re ready, your blood donation procedure will begin. Whole blood donation is the most common type of donation. This is because it offers the most flexibility. It can be transfused as whole blood or separated into red cells, platelets, and plasma for different recipients.
                            </p>
                        </div>
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                For a whole blood donation procedure:
                            </p>
                        </div>
                        <div class="col-md-12" style="margin-bottom: 30px">
                            <ol>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">You’ll be seated in a reclining chair. You can donate blood either sitting or lying down.</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">A small area of your arm will be cleaned. A sterile needle will then be inserted.</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">You’ll remain seated or lying down while a pint of your blood is drawn. This takes 8 to 10 minutes.</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">When a pint of blood has been collected, a staff member will remove the needle and bandage your arm.</li>
                            </ol>
                        </div>
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                Other types of donation include:
                            </p>
                        </div>
                        <div class="col-md-12" style="margin-bottom: 30px">
                            <ul>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Platelet Donation (Plateletpheresis)</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px"><a href="https://bit.ly/2KzPIan" style="color: #cc0000" target="_blank">Plasma Donation</a> (Plasmapheresis)</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Double Red Cell Donation</li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                These types of donations are performed using a process called apheresis. An apheresis machine is connected to both of your arms. It collects a small amount of blood and separates the components before returning the unused components back to you. This cycle is repeated several times over approximately two hours.
                            </p>
                        </div>
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                Once your donation is complete, you’ll be given a snack and a drink and be able to sit and rest for 10 or 15 minutes before you leave. If you feel faint or nauseous, you’ll be able to lie down until you feel better.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 intro-text">
                    <h2 style="font-size: 35px; font-weight: bold; margin-bottom: 20px; line-height: 40px">What to know before you donate</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                Here are some important things to know before you donate:
                            </p>
                        </div>
                        <div class="col-md-12" style="margin-bottom: 30px">
                            <ul>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">You need to be 17 or older to donate whole blood. Some states allow you to donate at 16 with parental consent.</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">You have to weigh at least 110 pounds and be in good health to donate.</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">You need to provide information about medical conditions and any medications you’re taking. These may affect your eligibility to donate blood.</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">You must wait at least eight weeks between whole blood donations and 16 weeks between double red cell donations.</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Platelet donations can be made every seven days, up to 24 times per year.</li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                The following are some suggestions to help you prepare for donating blood:
                            </p>
                        </div>
                        <div class="col-md-12" style="margin-bottom: 30px">
                            <ul>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Drink an extra 16 ounces of water before your appointment.</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Eat a healthy meal that’s low in fat.</li>
                                <li style="margin-left: 80px; font-size: 20px; color: #666666; line-height: 40px">Wear a short-sleeved shirt or a shirt with sleeves that are easy to roll up.</li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <p style="font-size: 20px">
                                Let the staff know if you have a preferred arm or vein and if you prefer to sit up or lie down. Listening to music, reading, or talking to someone else can help you relax during the donation process.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 intro-text">
                    <h2 style="font-size: 15px; font-weight: bold; color: #cc0000; margin-bottom: 20px; line-height: 40px">Sources:</h2>
                    <div class="row">
                        <div class="col-md-12" style="margin-bottom: 30px">
                            <ul>
                                <li style="margin-left: 50px; font-size: 15px; color: #666666; line-height: 25px">Blood need and blood supply. (n.d.).
                                    <p style="margin-bottom: 20px"><a href="https://rcblood.org/2RUP86t" style="color: #cc0000" target="_blank">https://www.redcrossblood.org/donate-blood/how-to-donate/how-blood-donations-help/blood-needs-blood-supply.html</a></p>
                                </li>
                                <li style="margin-left: 50px; font-size: 15px; color: #666666; line-height: 25px">The donation process. (n.d.).
                                    <p style="margin-bottom: 20px"><a href="https://rcblood.org/2V0cCJi" style="color: #cc0000" target="_blank">https://www.redcrossblood.org/donate-blood/blood-donation-process/donation-process-overview.html</a></p>
                                </li>
                                <li style="margin-left: 50px; font-size: 15px; color: #666666; line-height: 25px">Edgren G, et al. (2008). Donation frequency, iron loss, and risk of cancer among blood donors. DOI:
                                    <p style="margin-bottom: 20px"><a href="https://bit.ly/2VBMaFR" style="color: #cc0000" target="_blank">https://www.academic.oup.com/jnci/article/100/8/572/927859</a></p>
                                </li>
                                <li style="margin-left: 50px; font-size: 15px; color: #666666; line-height: 25px">Fonseca-Nunes A, et al. (2014). Iron and cancer risk — a systematic review and meta-analysis of the epidemiological evidence. DOI:
                                    <p style="margin-bottom: 20px"><a href="https://bit.ly/2v24hJp" style="color: #cc0000" target="_blank">http://www.cebp.aacrjournals.org/content/23/1/12</a></p>
                                </li>
                                <li style="margin-left: 50px; font-size: 15px; color: #666666; line-height: 25px">Information for student donors. (n.d.).
                                    <p style="margin-bottom: 20px"><a href="https://rcblood.org/2FuOSIP" style="color: #cc0000" target="_blank">https://www.redcrossblood.org/donate-blood/how-to-donate/info-for-student-donors.html</a></p>
                                </li>
                                <li style="margin-left: 50px; font-size: 15px; color: #666666; line-height: 25px">Kamhieh-Milz S, et al. (2015). Regular blood donation may help in the management of hypertension: An observational study on 292 blood donors.
                                    <p style="margin-bottom: 20px"><a href="https://bit.ly/2KxQuot" style="color: #cc0000" target="_blank">http://www.blutspendezurich.ch/Media/File/Journal%20Club/12.3.2016%20SONG%20jc%20trf13428.pdf</a></p>
                                </li>
                                <li style="margin-left: 50px; font-size: 15px; color: #666666; line-height: 25px">Reichel M. (2014) Give blood; give life.
                                    <p style="margin-bottom: 20px"><a href="https://bit.ly/2GjRzfp" style="color: #cc0000" target="_blank">http://www.sites.psu.edu/mreichel/wp-content/uploads/sites/38624/2016/04/ADVOCACY-PAPER.pdf</a></p>
                                </li>
                                <li style="margin-left: 50px; font-size: 15px; color: #666666; line-height: 25px">Robotham D, et al. (2012). Doing good? Altruism and wellbeing in an age of austerity.
                                    <p style="margin-bottom: 20px"><a href="https://bit.ly/2Z7Sfvw" style="color: #cc0000" target="_blank">https://www.mentalhealth.org.uk/publications/doing-good-altruism-and-wellbeing-age-austerity</a></p>
                                </li>
                                <li style="margin-left: 50px; font-size: 15px; color: #666666; line-height: 25px">Uche EI, et al. (2013). Lipid profile of regular blood donors. DOI:
                                    <p style="margin-bottom: 20px"><a href="https://bit.ly/2UQoTCO" style="color: #cc0000" target="_blank">https://www.dovepress.com/lipid-profile-of-regular-blood-donors-peer-reviewed-article-JBM</a></p>
                                </li>
                                <li style="margin-left: 50px; font-size: 15px; color: #666666; line-height: 25px">Yunce M, et al. (2016). One more health benefit of blood donation: Reduces acute-phase reactants, oxidants and increases antioxidant capacity. DOI:
                                    <p style="margin-bottom: 20px"><a href="https://bit.ly/2URVLuM" style="color: #cc0000" target="_blank">https://www.degruyter.com/view/j/jbcpp.2016.27.issue-6/jbcpp-2015-0111/jbcpp-2015-0111.xml</a></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Intro section end -->


<?php include_once ('../include/footer.php');?>