<?php include_once '../../view/include/header.php';
session_start();
//var_dump($_SESSION);
if (!empty($_SESSION))
{
//    echo 'Yes1';
//    if (!isset($_SESSION['LoginMsg']))
//    {
//        echo 'Yes2';
//        session_destroy();
////        header('location: index.php');
//    }
    if (!empty($_SESSION['PostUpdtMsg'])) {
        if (($_SESSION['PostUpdtMsg']) == 'success') {
            echo "<script>window.alert('Post Successfully Updated!')</script>";
            $_SESSION['PostUpdtMsg'] = '';
        }
//        if (($_SESSION['RegMsg']) == 'emailExists') {
//            echo "<script>window.alert('Email Already Registered!')</script>";
//            $_SESSION['RegMsg'] = '';
//        }
    }
    if (isset($_SESSION['LoginMsg']))
    {
//        echo 'Yes3';
//        if ($_SESSION['LoginMsg'] != 'success')
//        {
////            echo 'Yes4';
//            echo "<script>window.alert('Please Login First')</script>";
//            session_destroy();
//            header('location: ../../index.php');
//        }
        if ($_SESSION['LoginMsg'] == 'success')
        {
            include '../../src/Users.php';
            $object = new Users();
            $userEmail = $_SESSION['email'];
            $_SESSION['currentUserInfo'] = $object->currentUserInfo($userEmail);
            $_SESSION['allUserInfo'] = $object->allUserInfo();
//            var_dump($_GET['postID']);
//            if (!isset($_GET['postID']))
//                $_GET['postID'] = $_SESSION['postID'];
//            $_SESSION['postInfo'] = $object->singlePostInfo($_GET['postID']);
//            var_dump($_SESSION['postInfo']);
//            var_dump($_SESSION['postDuration'][0]['duration']);
//            var_dump($_SESSION['allUserInfo']);
//            echo $_SESSION['allUserInfo'][1]['name'];
//            echo $_SESSION['allUserInfo'][1]['bloodGroupNeeded'];
//            $countPost = $object->countPost();
//            $countUser = $object->countUser();
////            var_dump($count);
//            $colPost = 1;
//            $colUser = $countUser[0]['col'];
            $searchPostCol = $_SESSION['searchPostCount'][0]['col'];
//            echo $colPost;
//            var_dump($_SESSION['postInfo'][0]['bloodGroupNeeded']);

            $userFullName = $_SESSION['currentUserInfo'][0]['name'];
            $bloodGroupNeeded = $_SESSION['currentUserInfo'][0]['bloodGroupNeeded'];


        }
    }
    else{
//    echo 'Here';
        $_SESSION['LoginMsg']='loginFirst';
        header('location: ../../index.php');
    }
}
elseif (empty($_SESSION))
{
//    echo 'Here';
    $_SESSION['LoginMsg']='loginFirst';
    header('location: ../../index.php');
}
?>

<!--Navbar-->
<nav class="navbar navbar-expand-lg navbar-dark danger-color-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="index.php" target="_self"><img src="assets/img/BloodBook.png" class="rounded-circle z-depth-0 logo-size" alt="avatar image"></a>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <form class="form-inline mr-auto" action="view/admin/search.php" method="POST">
                <input required class="form-control" name="search" type="text" placeholder="Search" aria-label="Search">
            </form>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-5" aria-controls="navbarSupportedContent-5" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-5">
            <ul style="list-style: none" class="navbar-nav ml-auto nav-flex-icons">
                <li class="nav-item <?php if (isset($_GET['navhead']))if ($_GET['navhead'] == 'social') echo 'active'?>">
                    <a class="nav-link waves-effect waves-light" href="view/user/social.php?navhead=social">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item <?php if (isset($_GET['navhead']))if ($_GET['navhead'] == 'profile') echo 'active'?>">
                    <a class="nav-link waves-effect waves-light" href="view/user/profile.php?navhead=profile">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light" href="" onclick="return false">Donors</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light" onclick="return false">2
                        <i class="fas fa-envelope"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light" onclick="return false">10+
                        <i class="fas fa-bell"></i>
                    </a>
                </li>
                <?php if (!empty($_SESSION)):?>
                    <?php if (isset($_SESSION['LoginMsg'])):?>
                        <?php if ($_SESSION['LoginMsg'] == 'success'):?>
                            <li class="dropdown">
                                <a class="social-nav-propic nav-link waves-effect waves-light" id="navbarDropdownMenuLink-5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <img alt="" src="assets/propic/<?php
                                    if ($_SESSION['currentUserInfo'][0]['propic'] != NULL){
                                        $_SESSION['currentPic'] = $_SESSION['currentUserInfo'][0]['propic'];
                                        echo $_SESSION['currentUserInfo'][0]['propic'];
                                    }
                                    else
                                        echo 'blank.png';
                                    ?>
                                ">
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-secondary" aria-labelledby="navbarDropdownMenuLink-5">
                                    <h6 class="us-links">Settings</h6>
                                    <ul style="list-style: none" class="us-links">
                                        <li><a href="view/user/accountSettings.php?userID=" title="">Account Setting</a></li>
                                        <li><a href="" onclick="return false" title="">Privacy</a></li>
                                        <li><a href="" onclick="return false" title="">Faqs</a></li>
                                        <li><a href="" onclick="return false" title="">Terms &amp; Conditions</a></li>
                                    </ul>
                                    <h6 class="tc"><a href="view/admin/logout.php" title="">Logout</a></h6>
                                </div>
                            </li>
                        <?php endif;?>
                    <?php endif;?>
                <?php endif;?>
            </ul>
        </div>
    </div>
</nav>

<div class="container" id="autoload">
    <div class="row padding">
        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8  offset-xl-2 offset-lg-2 offset-md-2 offset-sm-2 offset-2 no-pd">
            <div class="main-ws-sec">
                <?php if ($searchPostCol>0):?>
                    <?php for ($i=0; $i<$searchPostCol; $i++):?>
                        <div class="posts-section">
                            <div class="post-bar">
                                <div class="post_topbar">
                                    <div class="usy-dt">
                                        <img src="assets/propic/<?php
                                        if ($_SESSION['searchResultPost'][$i]['propic'] != NULL)
                                            echo $_SESSION['searchResultPost'][$i]['propic'];
                                        else
                                            echo 'blank.png';
                                        ?>" class="" alt="avatar image">
                                        <div class="usy-name">
                                            <?php if ($_SESSION['searchResultPost'][$i]['email'] == $userEmail):?>
                                                <a href="view/user/profile.php?navhead=profile" target="_self"><h3><?php echo $_SESSION['searchResultPost'][$i]['name']?></h3></a>
                                            <?php endif;?>
                                            <?php if ($_SESSION['searchResultPost'][$i]['email'] != $userEmail):?>
                                                <a href="view/user/viewProfile.php?userID=<?php echo $_SESSION['searchResultPost'][$i]['userID'];?>" target="_self"><h3><?php echo $_SESSION['searchResultPost'][$i]['name']?></h3></a>
                                            <?php endif;?>
                                            <span>
                                        <i class="fas fa-clock fa-fw "></i>
                                        <?php
                                        $_SESSION['postDuration'] = $object->postDuration($_SESSION['searchResultPost'][$i]['usersEmail'], $_SESSION['searchResultPost'][$i]['postID']);
                                        $duration = $_SESSION['postDuration'][0]['duration'];
                                        if ($duration >= 60)
                                        {
                                            $duration = $duration / 60;
                                            if ($duration >= 60)
                                            {
                                                $duration = $duration / 60;
                                                if ($duration >= 24)
                                                {
                                                    $duration = $duration / 24;
                                                    if ($duration > 30)
                                                    {
                                                        $duration = $duration / 30;
                                                        if ($duration > 12)
                                                        {
                                                            $duration = $duration / 12;
                                                            if ($duration>1 && $duration<2)
                                                                echo (int)$duration.' year';
                                                            else
                                                                echo (int)$duration.' years';
                                                        }
                                                        else
                                                        {
                                                            if ($duration>1 && $duration<2)
                                                                echo (int)$duration.' month';
                                                            else
                                                                echo (int)$duration.' months';
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if ($duration>1 && $duration<2)
                                                            echo (int)$duration.' day';
                                                        else
                                                            echo (int)$duration.' days';
                                                    }
                                                }
                                                else
                                                {
                                                    if ($duration>1 && $duration<2)
                                                        echo (int)$duration.' hr';
                                                    else
                                                        echo (int)$duration.' hrs';
                                                }
                                            }
                                            else
                                            {
                                                if ($duration>1 && $duration<2)
                                                    echo (int)$duration.' min';
                                                else
                                                    echo (int)$duration.' mins';
                                            }
                                        }
                                        else
                                            echo (int)$duration.' sec';
                                        ?>
                                        ago
                                    </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="epi-sec">
                                    <ul style="list-style: none" class="descp">
                                        <li><i class="fas fa-globe-americas"></i></li>
                                        <span>Dhaka, Bangladesh</span>
                                    </ul>
                                    <ul style="list-style: none" class="bk-links">
                                        <li><a href="" onclick="return false" title=""><i class="far fa-bookmark"></i></a></li>
                                        <li><a href="" onclick="return false" title=""><i class="far fa-envelope"></i></a></li>
                                    </ul>
                                </div>
                                <div class="job_descp">
                                    <a href="view/user/viewPost.php?postID=<?php echo $_SESSION['searchResultPost'][$i]['postID']?>">
                                        <h3><?php echo $_SESSION['searchResultPost'][$i]['bloodGroupNeeded']?>(ve) Blood Needed</h3>
                                    </a>
                                    <!--                            <ul style="list-style: none" class="job-dt">-->
                                    <!--                            <li><a href="" title="">Full Time</a></li>-->
                                    <!--                            <li><span>$30 / hr</span></li>-->
                                    <!--                            </ul>-->
                                    <ul style="list-style: none" class="mandatory-post-info">
                                        <li>Donor Needed: <?php echo $_SESSION['searchResultPost'][$i]['donorNeeded']?></li>
                                        <li>Location: <?php echo $_SESSION['searchResultPost'][$i]['hospital']?></li>
                                        <li>Contact Person: <?php echo $_SESSION['searchResultPost'][$i]['contactPerson']?></li>
                                        <li>Contact Number: <?php echo $_SESSION['searchResultPost'][$i]['contactNumber']?></li>
                                    </ul>
                                    <?php if ($_SESSION['searchResultPost'][$i]['postDetails'] != NULL):?>
                                        <p><?php echo $_SESSION['searchResultPost'][$i]['postDetails'];?> <a href="" onclick="return false" title="">view more</a></p>
                                    <?php endif;?>
                                </div>

                                <!--                        Comment Section-->
                                <!--                        <div class="dropdown ed-opts">-->
                                <!--                            <a href="" onclick="return false" title="" class="dropdown-toggle" data-toggle="dropdown"-->
                                <!--                               aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v" data-toggle="dropdown"></i></a>-->
                                <!--                            <div class="dropdown-menu" aria-labelledby="">-->
                                <!--                                <button class="dropdown-item" type="button"  data-toggle="modal" data-target="#modalBloodDonateUpdateForm--><?php //echo $i?><!--">Edit Post</button>-->
                                <!--                                <button class="dropdown-item" type="button">Unsave Post</button>-->
                                <!--                                <button class="dropdown-item" type="button">Hide</button>-->
                                <!--                            </div>-->
                                <!--                        </div>-->
                            </div>
                        </div>
                    <?php endfor;?>
                <?php endif;?>
            </div><!--main-ws-sec end-->
        </div>
    </div>

</div>

<?php include_once '../../view/include/footer.php'?>
