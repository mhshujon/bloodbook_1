<?php include_once ('../include/header.php');

session_start();
//var_dump($_SESSION);
if (!empty($_SESSION))
{
//    echo 'Yes1';
//    if (!isset($_SESSION['LoginMsg']))
//    {
//        echo 'Yes2';
//        session_destroy();
////        header('location: index.php');
//    }
    if (!empty($_SESSION['PostUpdtMsg'])) {
        if (($_SESSION['PostUpdtMsg']) == 'success') {
            echo "<script>window.alert('Post Successfully Updated!')</script>";
            $_SESSION['PostUpdtMsg'] = '';
        }
//        if (($_SESSION['RegMsg']) == 'emailExists') {
//            echo "<script>window.alert('Email Already Registered!')</script>";
//            $_SESSION['RegMsg'] = '';
//        }
    }
    if (isset($_SESSION['LoginMsg']))
    {
//        echo 'Yes3';
//        if ($_SESSION['LoginMsg'] != 'success')
//        {
////            echo 'Yes4';
//            echo "<script>window.alert('Please Login First')</script>";
//            session_destroy();
//            header('location: ../../index.php');
//        }
        if ($_SESSION['LoginMsg'] == 'success')
        {
            include '../../src/Users.php';
            $object = new Users();
            $userEmail = $_SESSION['email'];
//            $_SESSION['allUserInfo'] = $object->allUserInfo();
            $_SESSION['currentUserInfo'] = $object->currentUserInfo($userEmail);
            $_SESSION['postInfo'] = $object->postInfo($userEmail);
//            var_dump($_SESSION['postInfo']);
//            var_dump($_SESSION['postDuration'][0]['duration']);
//            var_dump($_SESSION['allUserInfo']);
//            echo $_SESSION['allUserInfo'][1]['name'];
//            echo $_SESSION['allUserInfo'][1]['bloodGroup'];
            $countPost = $object->countPost();
            $countUser = $object->countUser();
//            var_dump($count);
            $colPost = $countPost[0]['col'];
            $colUser = $countUser[0]['col'];
//            echo $colPost;
//            var_dump($_SESSION['postInfo'][0]['bloodGroup']);

            $userFullName = $_SESSION['currentUserInfo'][0]['name'];
            $bloodGroup = $_SESSION['currentUserInfo'][0]['bloodGroup'];
            if (!isset($_GET['navhead']))
                $_GET['navhead'] = $_SESSION['navhead'];
        }
    }
    else{
//    echo 'Here';
        $_SESSION['LoginMsg']='loginFirst';
        header('location: ../../index.php');
    }
}
elseif (empty($_SESSION))
{
//    echo 'Here';
    $_SESSION['LoginMsg']='loginFirst';
    header('location: ../../index.php');
}

?>

    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark danger-color-dark fixed-top">
        <div class="container">
            <a class="navbar-brand" href="index.php" target="_self"><img src="assets/img/BloodBook.png" class="rounded-circle z-depth-0 logo-size" alt="avatar image"></a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <form class="form-inline mr-auto" action="view/admin/search.php" method="POST">
                    <input required class="form-control" name="search" type="text" placeholder="Search" aria-label="Search">
                </form>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-5" aria-controls="navbarSupportedContent-5" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent-5">
                <ul style="list-style: none" class="navbar-nav ml-auto nav-flex-icons">
                    <li class="nav-item <?php if (isset($_GET['navhead']))if ($_GET['navhead'] == 'social') echo 'active'?>">
                        <a class="nav-link waves-effect waves-light" href="view/user/social.php?navhead=social">Home
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item <?php if (isset($_GET['navhead']))if ($_GET['navhead'] == 'profile') echo 'active'?>">
                        <a class="nav-link waves-effect waves-light" href="view/user/profile.php?navhead=profile">Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link waves-effect waves-light" href="" onclick="return false">Donors</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link waves-effect waves-light" onclick="return false">2
                            <i class="fas fa-envelope"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link waves-effect waves-light" onclick="return false">10+
                            <i class="fas fa-bell"></i>
                        </a>
                    </li>
                    <?php if (!empty($_SESSION)):?>
                        <?php if (isset($_SESSION['LoginMsg'])):?>
                            <?php if ($_SESSION['LoginMsg'] == 'success'):?>
                                <li class="dropdown">
                                    <a class="social-nav-propic nav-link waves-effect waves-light" id="navbarDropdownMenuLink-5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <img alt="" src="assets/propic/<?php
                                        if ($_SESSION['currentUserInfo'][0]['propic'] != NULL){
                                            $_SESSION['currentPic'] = $_SESSION['currentUserInfo'][0]['propic'];
                                            echo $_SESSION['currentUserInfo'][0]['propic'];
                                        }
                                        else
                                            echo 'blank.png';
                                        ?>
                                ">
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-secondary" aria-labelledby="navbarDropdownMenuLink-5">
                                        <h6 class="us-links">Settings</h6>
                                        <ul style="list-style: none" class="us-links">
                                            <li><a href="view/user/accountSettings.php?userID=" title="">Account Setting</a></li>
                                            <li><a href="" onclick="return false" title="">Privacy</a></li>
                                            <li><a href="" onclick="return false" title="">Faqs</a></li>
                                            <li><a href="" onclick="return false" title="">Terms &amp; Conditions</a></li>
                                        </ul>
                                        <h6 class="tc"><a href="view/admin/logout.php" title="">Logout</a></h6>
                                    </div>
                                </li>
                            <?php endif;?>
                        <?php endif;?>
                    <?php endif;?>
                </ul>
            </div>
        </div>
    </nav>

<div class="container">
    <div class="col-md-12 offset-md-4" style="margin-top: 100px;">
        <h1 style="font-weight: bolder; font-size: 50px">Nothing Found</h1>
    </div>
</div>

<?php include_once '../../view/include/footer.php'?>