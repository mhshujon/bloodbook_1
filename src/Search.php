<?php
include ('Connection.php');

class Search extends Connection
{
    public function searchUser($userEmail){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `users` WHERE `email`='$userEmail'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function searchPost($bloodGroup){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `posts` AS P JOIN `users` AS U ON U.email = P.usersEmail WHERE `bloodGroupNeeded` = '$bloodGroup'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function postCount($bloodGroup){
        try{
            $stmt = $this->con->prepare("SELECT COUNT(*) AS col FROM `posts` WHERE `bloodGroupNeeded` = '$bloodGroup'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
}
