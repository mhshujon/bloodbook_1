<?php
include ('Connection.php');

class Users extends Connection
{
    private $userID;
    private $email;
    private $name;
    private $pass;
    private $gender;
    private $bloodGroup;
    private $following;
    private $followers;
    private $location;
    private $work_institution;
    private $fbLink;
    private $propic;
    private $availableToDonate = 'NO';

    public function set($data = array()){
        if (array_key_exists('userID', $data)) {
            $this->userID = $data['userID'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('pass', $data)) {
            $this->pass = $data['pass'];
        }
        if (array_key_exists('gender', $data)) {
            $this->gender = $data['gender'];
        }
        if (array_key_exists('bloodGroup', $data)) {
            $this->bloodGroup = $data['bloodGroup'];
        }
        if (array_key_exists('following', $data)) {
            $this->following = $data['following'];
        }
        if (array_key_exists('followers', $data)) {
            $this->followers = $data['followers'];
        }
        if (array_key_exists('location', $data)) {
            $this->location = $data['location'];
        }
        if (array_key_exists('work_institution', $data)) {
            $this->work_institution = $data['work_institution'];
        }
        if (array_key_exists('fbLink', $data)) {
            $this->fbLink = $data['fbLink'];
        }
        if (array_key_exists('propic', $data)) {
            $this->propic = $data['propic'];
        }
        if (array_key_exists('availableToDonate', $data)) {
            $this->availableToDonate = $data['availableToDonate'];
        }

//        var_dump($this);
//        return $this->name;
    }

    public function store(){
        try{
            $stmt = $this->con->prepare("INSERT INTO `users` (`userID`, `email`, `name`, `pass`,`gender`, `bloodGroup`, `following`, `followers`, `location`, `work_institution`, `fbLink`, `propic`,`availableToDonate`) VALUES (:userID, :email, :name, :pass, :gender, :bloodGroup, :following, :followers, :location, :work_institution, :fbLink, :propic,:availableToDonate)");
            $result =  $stmt->execute(array(
                ':userID' => $this->userID,
                ':email' => $this->email,
                ':name' => $this->name,
                ':pass' => $this->pass,
                ':gender' => $this->gender,
                ':bloodGroup' => $this->bloodGroup,
                ':following' => $this->following,
                ':followers' => $this->followers,
                ':location' => $this->location,
                ':work_institution' => $this->work_institution,
                ':fbLink' => $this->fbLink,
                ':propic' => $this->propic,
                ':availableToDonate' => $this->availableToDonate
            ));

//            var_dump('this->name');
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
//            if($result){
//                header('location:index.php');
//                echo $this->name;
//            }
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function allUserInfo(){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `users` ORDER BY `userID` DESC");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }

    }public function singleUserInfo($userID){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `users` WHERE `userID` = '$userID'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function currentUserInfo($email){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `users` WHERE  `email` = '$email'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function postInfo(){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `posts` ORDER BY `postID` DESC");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
    public function singlePostInfo($postID){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `posts` WHERE `postID` = '$postID' ORDER BY `postID` DESC");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function currentUserPostInfo($email){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `posts` WHERE `usersEmail` = '$email' ORDER BY `postID` DESC");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function commentInfo($postID){
        try{
            $stmt = $this->con->prepare("SELECT `commentID`, `name`, `comments`, `propic` FROM `comments` AS C JOIN `users` AS U ON U.email = C.usersEmail WHERE `postID` = '$postID' ORDER BY `commentID` DESC");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function postDuration($email, $postID){
        try{
            $stmt = $this->con->prepare("SELECT TIMESTAMPDIFF(SECOND, datetime,CURRENT_TIMESTAMP()) AS duration FROM `posts` WHERE `postID`='$postID' AND `usersEmail`='$email' ORDER BY `postID` DESC");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
    public function commentDuration($commentID, $postID){
        try{
            $stmt = $this->con->prepare("SELECT TIMESTAMPDIFF(SECOND, datetime,CURRENT_TIMESTAMP()) AS duration FROM `comments` WHERE `commentID` = '$commentID' AND `postID`='$postID' ORDER BY `postID` DESC");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function login($email, $pass){
        try{
            $stmt = $this->con->prepare("SELECT email, pass FROM `users` WHERE `email`='$email' AND `pass`='$pass'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function countPost(){
        try{
            $stmt = $this->con->prepare("SELECT COUNT(*) AS col FROM `posts`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function countComment($postID){
        try{
            $stmt = $this->con->prepare("SELECT COUNT(*) AS col FROM `comments` WHERE `postID` = '$postID'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function countUser(){
        try{
            $stmt = $this->con->prepare("SELECT COUNT(*) AS col FROM `users`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function countLikes($postID){
        try{
            $stmt = $this->con->prepare("SELECT COUNT(*) AS totalLikes FROM `likes` WHERE `postID` = '$postID'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function doneLike($postID, $userID){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `likes` WHERE `postID` = '$postID' AND `userID` = '$userID'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function emailValidation($email){
        try{
            $stmt = $this->con->prepare("SELECT `email` FROM `users` WHERE `email`='$email'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function passwordValidation($pass){
        try{
            $stmt = $this->con->prepare("SELECT `pass` FROM `users` WHERE `pass`='$pass'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

//    public function view($id){
//        try{
//            $stmt = $this->con->prepare("SELECT * FROM `users` WHERE id=:id");
//            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
//            $stmt->execute();
//            return $stmt->fetch(PDO::FETCH_ASSOC);
//
//        }catch (PDOException $e) {
//            echo "There is some problem in connection: " . $e->getMessage();
//        }
//    }
//
//    public function delete($id){
//        try{
//            $stmt = $this->con->prepare("DELETE FROM `users` WHERE id=:id");
//            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
//            $stmt->execute();
//            if($stmt){
//                $_SESSION['delete'] = 'Data successfully Deleted !!';
//                header('location:index.php');
//            }
//
//        }catch (PDOException $e) {
//            echo "There is some problem in connection: " . $e->getMessage();
//        }
//    }

    public function updateProfile(){
        try{
            $stmt = $this->con->prepare("UPDATE `dbserver`.`users` SET `email` = :email, `name` = :name, `pass` = :pass, `gender` = :gender, `bloodGroup` = :bloodGroup, `following` = :following, `followers` = :followers, `location` = :location, `work_institution` = :work_institution, `fbLink` = :fbLink, `propic` = :propic,`availableToDonate` = :availableToDonate WHERE `email` = :email;");

            $result =  $stmt->execute(array(
                ':email' => $this->email,
                ':name' => $this->name,
                ':pass' => $this->pass,
                ':gender' => $this->gender,
                ':bloodGroup' => $this->bloodGroup,
                ':following' => $this->following,
                ':followers' => $this->followers,
                ':location' => $this->location,
                ':work_institution' => $this->work_institution,
                ':fbLink' => $this->fbLink,
                ':propic' => $this->propic,
                ':availableToDonate' => $this->availableToDonate
            ));
            echo "\nPDOStatement::errorInfo():\n";
            $arr = $stmt->errorInfo();
            print_r($arr);
//            if($result){
////                echo 'Yes'.$this->email;
//            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
}
