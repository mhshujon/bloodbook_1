<?php
include ('Connection.php');

class Likes extends Connection
{
    private $postID;
    private $userID;

    public function set($data = array()){
        if (array_key_exists('postID', $data)) {
            $this->postID = $data['postID'];
        }
        if (array_key_exists('userID', $data)) {
            $this->userID = $data['userID'];
        }
//        var_dump($this);
//        return $this->userID;
    }

    public function store(){
        try{
            $stmt = $this->con->prepare("INSERT INTO `likes` (`postID`, `userID`) VALUES (:postID, :userID)");
            $result =  $stmt->execute(array(
                ':postID' => $this->postID,
                ':userID' => $this->userID
            ));

//            var_dump('this->userID');
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
//            if($result){
//                header('postDetails:index.php');
//                echo $this->userID;
//            }
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function delete($postID, $userID){
        try{
            $stmt = $this->con->prepare("DELETE FROM `likes` WHERE `postID`='$postID' AND `userID`='$userID'");
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr1 = $stmt1->errorInfo();
//            print_r($arr1);
//            if($stmt){
//                $_SESSION['delete'] = 'Data successfully Deleted !!';
//                header('postDetails:index.php');
//            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
}