-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2019 at 08:15 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbserver`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `commentID` int(11) NOT NULL,
  `postID` int(11) NOT NULL,
  `usersEmail` varchar(50) NOT NULL,
  `comments` varchar(255) NOT NULL,
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`commentID`, `postID`, `usersEmail`, `comments`, `datetime`) VALUES
(81, 56, 'avirup@email.com', 'Anyone?', '2019-04-11 18:04:57'),
(82, 56, 'avirup@email.com', 'its urgent', '2019-04-11 18:06:25'),
(83, 56, 'mhshujon@gmail.com', 'contact this number > 01302475984', '2019-04-11 18:07:39'),
(84, 56, 'avirup@email.com', 'Thanks', '2019-04-11 18:50:02'),
(85, 57, 'totusi96@gmail.com', 'khub valo vaiya', '2019-04-12 04:24:16'),
(86, 60, 'totusi96@gmail.com', '123 mic testing..', '2019-04-12 04:45:18'),
(87, 60, 'mhshujon@gmail.com', 'testing done :D ', '2019-04-12 05:49:43'),
(88, 64, 'mubasser95@gmail.com', 'anyone', '2019-04-12 06:22:03'),
(89, 60, 'mubasser95@gmail.com', 'when???', '2019-04-12 06:23:49'),
(90, 64, 'mhshujon@gmail.com', '?', '2019-04-12 12:05:52'),
(91, 60, 'sushi.sadaf@gmail.com', 'I am interested. Do you need blood within today?', '2019-04-13 02:23:45'),
(92, 66, 'moumitakabir13@gmail.com', '...', '2019-04-13 04:35:17'),
(93, 66, 'mhshujon@gmail.com', 'ðŸ˜', '2019-04-13 15:15:19'),
(94, 60, 'mhshujon@gmail.com', 'still needed?', '2019-04-18 12:23:48');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `likeID` int(11) NOT NULL,
  `postID` int(11) NOT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`likeID`, `postID`, `userID`) VALUES
(42, 66, 1),
(43, 64, 1),
(44, 62, 1),
(47, 65, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `postID` int(11) NOT NULL,
  `usersEmail` varchar(50) NOT NULL,
  `bloodGroupNeeded` varchar(10) NOT NULL,
  `donorNeeded` text NOT NULL,
  `hospital` varchar(50) NOT NULL,
  `contactPerson` varchar(50) NOT NULL,
  `contactNumber` varchar(50) NOT NULL,
  `postDetails` varchar(255) DEFAULT NULL,
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`postID`, `usersEmail`, `bloodGroupNeeded`, `donorNeeded`, `hospital`, `contactPerson`, `contactNumber`, `postDetails`, `datetime`) VALUES
(56, 'avirup@email.com', 'O+', '2', 'Apollo Hospital', 'Avirup', '01685498752', NULL, '2019-04-11 18:04:05'),
(57, 'habid96@gmail.com', 'O+', '1', 'Square Hospital ', 'Husnul Abid', '19653861XX', NULL, '2019-04-11 22:22:15'),
(60, 'totusi96@gmail.com', 'O+', '3', 'Dhaka Medical College', 'ami', '01919567475', NULL, '2019-04-12 04:43:27'),
(62, 'onygame112@gmail.com', 'O+', '1', 'Square Hospital', 'Ony', '01822593000', NULL, '2019-04-12 04:46:20'),
(63, 'mhshujon@gmail.com', 'O+', '1', 'Dhaka Medical College & Hospital', 'Monir', '01683852548', NULL, '2019-04-12 05:52:20'),
(64, 'mubasser95@gmail.com', 'O+', '2', 'Dhaka Medical College & Hospital', 'Md. Monir Hossain', '01683852548', NULL, '2019-04-12 06:21:30'),
(65, 'sushi.sadaf@gmail.com', 'AB-', '4', 'Mymensingh Medical College', 'Tehzib Sadaf ', '01999999999', NULL, '2019-04-13 02:25:29'),
(66, 'rafi@email.com', 'A+', '1', 'apollo', 'Rezuanur', '01656545454', NULL, '2019-04-13 04:34:25'),
(67, 'mhshujon@gmail.com', 'A-', '2', 'Square Hospital', 'Monir', '01645782569', NULL, '2019-04-14 05:49:20'),
(68, 'alaminalifaa7@gmail.com', 'AB+', '2', 'Ty', 'At', '635558', NULL, '2019-04-19 04:27:58');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userID` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `bloodGroup` varchar(10) NOT NULL,
  `following` varchar(50) DEFAULT NULL,
  `followers` varchar(50) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `work_institution` varchar(255) DEFAULT NULL,
  `fbLink` varchar(500) DEFAULT NULL,
  `propic` varchar(20) DEFAULT NULL,
  `availableToDonate` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userID`, `email`, `name`, `pass`, `gender`, `bloodGroup`, `following`, `followers`, `location`, `work_institution`, `fbLink`, `propic`, `availableToDonate`) VALUES
(15, 'alaminalifaa7@gmail.com', 'Al amin alif', '654321', NULL, 'A+', NULL, NULL, NULL, NULL, NULL, NULL, 'NO'),
(12, 'avi@email.com', 'Avirup Sarkar', '123', 'Male', 'A+', NULL, NULL, 'Mohammadpur', 'United International University', '', NULL, 'NO'),
(2, 'avirup@email.com', 'Avirup Sarker', '123', 'Male', 'A+', NULL, NULL, 'Satarkul, Badda, Dhaka', 'United Internation University', 'https://www.facebook.com/avirup.sarker', NULL, 'YES'),
(4, 'habid96@gmail.com', 'Husnul Abid', 'hudai1234', NULL, 'O+', NULL, NULL, NULL, NULL, NULL, NULL, 'NO'),
(8, 'hasnahenamow@gmail.com', 'Hasna Hena Mow', 'Mow123..', NULL, 'A+', NULL, NULL, NULL, NULL, NULL, NULL, 'NO'),
(1, 'mhshujon@gmail.com', 'Md Monir Hossain', '123', 'Male', 'AB+', NULL, NULL, 'Lalbag, Dhaka', 'United Internation University', 'https://www.facebook.com/mhshujon07', '75a487ab5c.jpg', 'YES'),
(11, 'moumitakabir13@gmail.com', 'Moumita', 'ammuamu', 'Female', 'O+', NULL, NULL, 'Dhaka', '', '', NULL, 'YES'),
(7, 'mubasser95@gmail.com', 'Mubasser Ahmed ', 'qwerty', 'Male', 'O+', NULL, NULL, 'Lalbag, Dhaka', 'North South University', 'https://www.facebook.com/mubasserahmed.alex1', NULL, 'NO'),
(9, 'nifat.promee12@gmail.com', 'nifat promee', 'Winnepooh', 'Female', 'B+', NULL, NULL, 'Mirpur', 'Student', '', NULL, 'NO'),
(6, 'onygame112@gmail.com', 'Tafsir Ony', '1234', 'Male', 'O+', NULL, NULL, 'Dhaka', 'UIU', 'https://fb.me/tafsirony', NULL, 'YES'),
(13, 'rabby.bca@gmail.com', 'Md. Fazlay Rabby', '7450550', NULL, 'A+', NULL, NULL, NULL, NULL, NULL, NULL, 'NO'),
(3, 'rafi@email.com', 'Rezuanur Rafi', '123', 'Male', 'A+', NULL, NULL, 'Mohakhali DOHS, Dhaka', 'United Internation University', 'https://www.facebook.com/rezuanur.rafy', NULL, 'YES'),
(14, 'sagor8@gmail.com', 'sagor sarker', '1234', NULL, 'A+', NULL, NULL, NULL, NULL, NULL, NULL, 'NO'),
(16, 'sk.zahir17@gmail.com', 'Sk. Zahir', '01616583518', 'Male', 'B+', NULL, NULL, 'Tejgaon, Nakhalpara, Dhaka', 'Green Life College of Nursing', 'https://www.facebook.com/sk.zahir.18', NULL, 'YES'),
(10, 'sushi.sadaf@gmail.com', 'Tehzib Sadaf', 'qwerty1234', NULL, 'AB+', NULL, NULL, NULL, NULL, NULL, NULL, 'NO'),
(5, 'totusi96@gmail.com', 'Tahrima Oannahary', 'amibloodditeparina', 'Female', 'O+', NULL, NULL, 'Dhaka', 'Nai', 'https://www.facebook.com/tahrima.tusi', NULL, 'NO');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`commentID`,`postID`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`likeID`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`postID`,`usersEmail`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`email`),
  ADD UNIQUE KEY `UNIQUE` (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `commentID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `likeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `postID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
